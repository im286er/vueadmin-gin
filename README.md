# vueadmin后台管理系统
------------------------

<img src="./demo.png" width="100%"/>  

## 强调
本系统基于vueadmin gin版本生成的演示系统，不具备生成器完整功能

#### 项目预览
[http://gin-admin.whpj.vip](http://gin.whpj.vip)


#### 官网地址
[http://www.vueadmin.net](http://www.vueadmin.net)


#### 技术栈
gin + gorm + vue2 + element



#### 说明

>  数据库配置 config.yml

>  后台登录账号：admin 密码：6yhn7ujm



#### 完成功能
```
- [x] 登录 -- 完成
- [x] 路由拦截 -- 完成
- [x] 商品管理（增加、编辑、搜索、删除） -- 完成
- [x] 角色管理（增加、编辑、搜索、删除、权限管理） -- 完成
- [x] 用户管理（增加、编辑、搜索、删除） -- 完成

- 定时任务
- 单点登录
- excel导入、导出
- 阿里云、七牛云oss 上传 以及客户端直传（config.yml 直接配置即可）
```


#### 前端运行 进入ui目录
------------------------

``` 
# 安装依赖包
npm install

# 启动命令
npm run serve

# 打包命令
npm run build

# 配置接口地址
进入 ui/src/api/request.js 配置服务端接口地址 默认已经设置好

```

#### 服务端运行
------------------------

``` 
# 安装依赖包
go mod tidy

# 启动项目
go run main.go

```
