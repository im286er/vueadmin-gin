package boot

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"github.com/vueadmin/global"
	"github.com/vueadmin/initialize"
	"github.com/vueadmin/pkg/event"
	"github.com/vueadmin/utils/valid"
	"os"
)

// 启动执行
func init() {
	workDir, _ := os.Getwd()
	v := viper.New()
	v.SetConfigName("config")
	v.SetConfigType("yml")
	v.AddConfigPath(workDir)
	if err := v.ReadInConfig(); err != nil {
		panic(err)
	}

	v.WatchConfig()

	v.OnConfigChange(func(e fsnotify.Event) {
		if err := v.Unmarshal(&global.CONF); err != nil {
			fmt.Println(err)
		}
	})

	if err := v.Unmarshal(&global.CONF); err != nil {
		fmt.Println(err)
	}

	//日志全局对象
	global.LOG = initialize.GetLogClient()

	//mysql全局对象
	global.DB = initialize.GetMysqlClient(global.CONF.Db.Dsn())

	//访问日志全局对象
	if global.CONF.System.AccessLogStatus {
		global.ACCESSLOG = initialize.GetAccessLogClient()
	}
	//redis全局链接
	if global.CONF.Cache.Driver == "redis" {
		global.REDIS = initialize.GetRedisClient(global.CONF.Redis)
	}

	//启动定时任务
	initialize.InitCrontab()
	//注册事件
	event.RegisterEvent()
	//注册验证器
	valid.InitValidator()

}
