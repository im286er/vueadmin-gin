package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/vueadmin/boot"
	"github.com/vueadmin/global"
	"github.com/vueadmin/initialize"
	"net/http"
	"os"
	"time"
)

func main() {
	gin.SetMode(global.CONF.Server.RunMode)
	router := initialize.Routers()
	server := &http.Server{
		Addr:           fmt.Sprintf(":%d", global.CONF.Server.HttpPort),
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	if err := server.ListenAndServe(); err != nil {
		fmt.Println("启动失败：", err)
		os.Exit(1)
	}
}
