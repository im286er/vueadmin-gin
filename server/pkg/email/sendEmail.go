package email

import (
	"github.com/vueadmin/global"
	"gopkg.in/gomail.v2"
)

//发送邮件
func SendMail(mailTo []string, subject string, body string) error {
	m := gomail.NewMessage(
		gomail.SetEncoding(gomail.Base64),
	)
	m.SetHeader("From", m.FormatAddress(global.CONF.Email.User, global.CONF.Email.Name))
	m.SetHeader("To", mailTo...)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)

	d := gomail.NewDialer(global.CONF.Email.Smtp, global.CONF.Email.Port, global.CONF.Email.User, global.CONF.Email.Pwd)
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}
