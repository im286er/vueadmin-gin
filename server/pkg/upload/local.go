package upload

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils"
	"mime/multipart"
)

var Local = new(local)

type local struct{}

// 本地上传
func (local) Upload(file *multipart.FileHeader, fileName string, c *gin.Context) string {
	if utils.CreatePath(utils.GetSavePath(c)) {
		if err := c.SaveUploadedFile(file, global.CONF.AccessPath+fileName); err != nil {
			panic(err)
		}
	}
	return fileName
}
