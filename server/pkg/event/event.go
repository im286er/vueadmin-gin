package event

import (
	"github.com/gookit/event"
	"github.com/vueadmin/pkg/listen"
)

// 注册事件监听器
func RegisterEvent() {
	event.On("adminLoginEvent", event.ListenerFunc(listen.AdminLoginEventListen), event.Normal)     //后台登陆日志
	event.On("adminLoginEvent", event.ListenerFunc(listen.AdminSsoLoginListen), event.Normal)       //后台单点登录
	event.On("exceptionLogEvent", event.ListenerFunc(listen.ExceptionLogEventListen), event.Normal) //异常日志
	event.On("afterUploadEvent", event.ListenerFunc(listen.CreateSaveFilepath), event.Normal)       //文件上传后置钩子 写入库
	event.On("afterUploadEvent", event.ListenerFunc(listen.ThumbImg), event.Normal)                 //生成缩略图
}
