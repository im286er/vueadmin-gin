package captcha

import (
	"github.com/vueadmin/global"
	"math/rand"
	"time"

	"github.com/mojocn/base64Captcha"
)

type configJsonBody struct {
	Id            string
	CaptchaType   string
	VerifyValue   string
	DriverAudio   *base64Captcha.DriverAudio
	DriverString  *base64Captcha.DriverString
	DriverChinese *base64Captcha.DriverChinese
	DriverMath    *base64Captcha.DriverMath
	DriverDigit   *base64Captcha.DriverDigit
}

var store base64Captcha.Store

func init() {
	rand.Seed(time.Now().Unix())
}

// 获取验证码
func Generate() map[string]interface{} {
	param := configJsonBody{
		Id:          "",
		CaptchaType: "digit",
		VerifyValue: "",
		DriverDigit: &base64Captcha.DriverDigit{
			Height:   global.CONF.Captcha.Height,
			Width:    global.CONF.Captcha.Width,
			Length:   global.CONF.Captcha.Length,
			MaxSkew:  0.1,
			DotCount: 80,
		},
	}
	var driver base64Captcha.Driver

	switch param.CaptchaType {
	case "audio":
		driver = param.DriverAudio
	case "string":
		driver = param.DriverString.ConvertFonts()
	case "math":
		driver = param.DriverMath.ConvertFonts()
	case "chinese":
		driver = param.DriverChinese.ConvertFonts()
	default:
		driver = param.DriverDigit
	}

	if global.CONF.Cache.Driver == "redis" {
		store = RedisStore{}
	} else {
		store = base64Captcha.DefaultMemStore
	}
	c := base64Captcha.NewCaptcha(driver, store)

	id, base64, _ := c.Generate()

	data := make(map[string]interface{})
	data["key"] = id
	data["base64"] = base64

	return data
}

// 验证
func Verify(id string, VerifyValue string) (res bool) {
	param := configJsonBody{
		Id:          id,
		CaptchaType: "digit",
		VerifyValue: VerifyValue,
		DriverDigit: &base64Captcha.DriverDigit{
			Height:   global.CONF.Captcha.Height,
			Width:    global.CONF.Captcha.Width,
			Length:   global.CONF.Captcha.Length,
			MaxSkew:  0.7,
			DotCount: 80,
		},
	}
	if global.CONF.Cache.Driver == "redis" {
		store = RedisStore{}
	} else {
		store = base64Captcha.DefaultMemStore
	}
	return store.Verify(param.Id, param.VerifyValue, true)
}
