package qrcode

import (
	"encoding/base64"
	"fmt"
	"github.com/skip2/go-qrcode"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils"
)

// 生成二维码 保存到 public/uploads/qrcode目录
func CreateQrcode(content string, size int, filename string) error {
	qr, err := qrcode.New(content, qrcode.Medium)
	if err != nil {
		panic(err)
	}
	filepath := fmt.Sprintf("%s/%s/qrcode", global.CONF.Upload.AccessPath, global.CONF.Upload.UploadPath)
	if err := utils.CreateDir(filepath); err != nil {
		panic(err)
	}
	if err := qr.WriteFile(size, filepath+"/"+filename); err != nil {
		return err
	}
	return nil
}

// 返回base64的二维码
func QrcodeBase64(content string, size int) string {
	qr, err := qrcode.New(content, qrcode.Medium)
	if err != nil {
		panic(err)
	}
	data, err2 := qr.PNG(size)
	if err2 != nil {
		panic(err2)
	}

	str := base64.StdEncoding.EncodeToString(data)
	str = "data:image/png;base64," + str
	return str
}
