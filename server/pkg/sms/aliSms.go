package sms

import (
	"github.com/alibabacloud-go/darabonba-openapi/client"
	dysmsapi20170525 "github.com/alibabacloud-go/dysmsapi-20170525/v2/client"
	"github.com/vueadmin/global"
)

// 发送阿里云短信验证码
func SendAliSms(mobile string, verify string) error {
	accessKeyId := global.CONF.AliSms.AccessKeyId
	accessKeySecret := global.CONF.AliSms.AccesskeySecrect
	endpoint := "dysmsapi.aliyuncs.com"
	c := &client.Config{AccessKeyId: &accessKeyId, AccessKeySecret: &accessKeySecret, Endpoint: &endpoint}

	newClient, err := dysmsapi20170525.NewClient(c)
	if err != nil {
		panic(err)
	}
	phoneNumber := mobile
	templateCode := global.CONF.AliSms.TempCode
	signName := global.CONF.AliSms.SignName
	code := "{\"code\":" + verify + "}"
	request := &dysmsapi20170525.SendSmsRequest{
		PhoneNumbers:  &phoneNumber,
		TemplateCode:  &templateCode,
		SignName:      &signName,
		TemplateParam: &code,
	}
	sms, err := newClient.SendSms(request)
	if err != nil {
		return err
	}
	if *sms.StatusCode == 200 {
		return nil
	}
	return nil
}
