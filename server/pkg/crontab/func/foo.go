package _func

import (
	"fmt"
	"github.com/vueadmin/utils/conv"
)

func Foo() {
	fmt.Println(conv.NowDateTime())
}
