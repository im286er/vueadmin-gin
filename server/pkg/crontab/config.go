package crontab

import (
	"github.com/vueadmin/global"
	_func "github.com/vueadmin/pkg/crontab/func"
)

// 初始化定时任务，添加进入map
func init() {
	AddTask("foo", _func.Foo) //测试的定时任务
}

var taskMap = make(map[string]func())

func AddTask(invokeTarget string, run func()) {
	taskMap[invokeTarget] = run
}

func GetTask(invokeTarget string) (run func()) {
	return taskMap[invokeTarget]
}

func IsExistFunc(invokeTarget string) (ok bool) {
	_, ok = taskMap[invokeTarget]
	return
}

func IsExistCron(invokeTarget uint) (ok bool) {
	_, ok = cronMap[invokeTarget]
	return
}

type Crontab struct {
	CrontabId   uint   `gorm:"primaryKey" json:"crontab_id"` //编号
	Name        string `json:"name"`                         //任务名称
	Type        int    `json:"type"`                         //任务类型
	Rule        string `json:"rule"`                         //规则
	Status      int8   `json:"status"`                       //状态
	Target      string `json:"target"`                       //调用目标
	Remark      string `json:"remark"`                       //描述
	LastRunTime int64  `json:"last_run_time"`                //最后执行时间
}

func (Crontab) TableName() string {
	return global.CONF.Db.Prefix + "crontab"
}
