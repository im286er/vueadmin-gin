package listen

import (
	"github.com/gin-gonic/gin"
	"github.com/gookit/event"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils"
	"github.com/vueadmin/utils/conv"
	"strings"
)

// 监听登陆，写入登录日志
func AdminLoginEventListen(e event.Event) error {
	handle := e.Data()["handle"].(*gin.Context)

	var data model.AdminLog
	data.ApplicationName = strings.Split(strings.TrimLeft(handle.Request.RequestURI, "/"), "/")[0]
	data.Username = e.Data()["user"].(string)
	data.Ip = utils.GetClientIP(handle)
	data.Url = "http://" + handle.Request.Host + handle.Request.RequestURI
	data.Useragent = handle.Request.UserAgent()
	data.Type = 1
	data.CreateTime = conv.NowUnix()

	if err := global.DB.Create(&data).Error; err != nil {
		global.LOG.Error(err.Error())
	}
	return nil
}
