package listen

import (
	"github.com/gookit/event"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

// 图片上传记录写入库
func CreateSaveFilepath(e event.Event) error {
	data := make(map[string]interface{})
	data["filepath"] = e.Data()["filepath"]
	data["hash"] = e.Data()["hash"]
	data["create_time"] = conv.NowUnix()
	data["disk"] = global.CONF.Disks

	if err := global.DB.Table(global.CONF.Db.Prefix + "file").Create(&data).Error; err != nil {
		panic(err)
	}
	return nil
}
