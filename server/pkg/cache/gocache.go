package cache

import (
	"github.com/patrickmn/go-cache"
	"github.com/vueadmin/utils/conv"
	"strings"
	"time"
)

var gocacheHandle = cache.New(5*time.Minute, 10*time.Minute)

var Gocache = new(gochache)

type gochache struct{}

// 缓存操作
func (p gochache) Remember(key string, ttl time.Duration, f func() string) string {
	res := p.Get(key)
	if res != "" {
		return res
	} else {
		res = f()
		p.Set(key, res, ttl)
	}
	return res
}

// 设置缓存，这个是适配cache Interface接口 专门设立的
func (p gochache) Set(key string, val string, expiretime time.Duration) error {
	gocacheHandle.Set(key, val, expiretime)
	return nil
}

// 获取缓存，这个是适配cache Interface接口 专门设立的
func (p gochache) Get(key string) string {
	value, ok := gocacheHandle.Get(key)
	if ok {
		return conv.String(value)
	}
	return ""
}

// 设置缓存
func (p gochache) Setx(key string, val interface{}, expiretime time.Duration) error {
	gocacheHandle.Set(key, val, expiretime)
	return nil
}

// 获取缓存
func (p gochache) Getx(key string) interface{} {
	value, ok := gocacheHandle.Get(key)
	if ok {
		return value
	}
	return nil
}

// 根据键名删除缓存
func (p gochache) Delete(key string) int64 {
	gocacheHandle.Delete(key)
	return 1
}

// 清空缓存
func (p gochache) Flush() {
	gocacheHandle.Flush()
}

// 模糊匹配key 批量删除
func (p gochache) DeleteByLike(key string) int64 {
	keysToDelete := make([]string, 0)
	for k := range gocacheHandle.Items() {
		if strings.HasPrefix(k, key) {
			keysToDelete = append(keysToDelete, k)
		}
	}
	for _, k := range keysToDelete {
		gocacheHandle.Delete(k)
	}
	return 1
}
