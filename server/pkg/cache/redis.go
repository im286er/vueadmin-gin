package cache

import (
	"context"
	"github.com/vueadmin/global"
	"go.uber.org/zap"
	"time"
)

var Redis = new(redis)

type redis struct{}

// 缓存操作
func (p redis) Remember(key string, ttl time.Duration, f func() string) string {
	res := p.Get(key)
	if res != "" {
		return res
	} else {
		res = f()
		p.Set(key, res, ttl)
	}
	return res
}

// 设置缓存
func (p redis) Set(key string, data string, expiration time.Duration) error {
	if err := global.REDIS.Set(context.Background(), key, data, expiration).Err(); err != nil {
		global.LOG.Error("[缓存异常：]",
			zap.String("key", key),
			zap.Any("data", data),
		)
	}
	return nil
}

// 获取缓存
func (p redis) Get(key string) string {
	res, _ := global.REDIS.Get(context.Background(), key).Result()
	return res
}

// hash表获取全部
func (p redis) HGetAll(key string) map[string]string {
	res, _ := global.REDIS.HGetAll(context.Background(), key).Result()
	return res
}

// 获取hash表单个值
func (p redis) HGet(key string, field string) string {
	res, _ := global.REDIS.HGet(context.Background(), key, field).Result()
	return res
}

// hash表操作
func (p redis) HSet(key string, field string, value string) {
	global.REDIS.HSet(context.Background(), key, field, value).Result()
}

// hash表操作
func (p redis) HDelete(key string, field string) {
	global.REDIS.HDel(context.Background(), key, field).Result()
}

// 删除缓存
func (p redis) Delete(key string) int64 {
	res, _ := global.REDIS.Del(context.Background(), key).Result()
	return res
}

// 模糊匹配key 批量删除
func (p redis) DeleteByLike(key string) int64 {
	keys, _ := global.REDIS.Keys(context.Background(), key).Result()
	if len(keys) > 0 {
		res, _ := global.REDIS.Del(context.Background(), keys...).Result()
		return res
	}
	return 0
}
