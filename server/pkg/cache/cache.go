package cache

import (
	"github.com/vueadmin/global"
	"time"
)

// 缓存接口
type cacheInterface interface {
	Set(key string, val string, expiretime time.Duration) error
	Get(key string) string
	Delete(key string) int64
	DeleteByLike(key string) int64
	Remember(key string, ttl time.Duration, f func() string) string
}

// 返回接口实例
func New(driver ...string) cacheInterface {
	if global.CONF.Cache.Driver == "gocache" || (len(driver) > 0 && driver[0] == "gocache") {
		return Gocache
	} else {
		return Redis
	}
}
