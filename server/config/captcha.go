package config

// 验证码配置
type Captcha struct {
	Length int `mapstructure:"length" yaml:"length"` // 验证码长度
	Width  int `mapstructure:"width" yaml:"width"`   // 验证码宽度
	Height int `mapstructure:"height" yaml:"height"` // 验证码高度
}
