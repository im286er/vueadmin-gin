package config

// jwt配置
type Jwt struct {
	Adminjwt
	Apijwt
}

// adminjwt配置
type Adminjwt struct {
	SigningKey  string `mapstructure:"signing-key" yaml:"signing-key"`   // jwt签名
	ExpiresTime int64  `mapstructure:"expires-time" yaml:"expires-time"` // 过期时间
	Aud         string `mapstructure:"aud" yaml:"aud"`                   //接受者
	Issuer      string `mapstructure:"issuer" yaml:"issuer"`             // 签发者
}

// apijwt配置
type Apijwt struct {
	SigningKey  string `mapstructure:"signing-key" yaml:"signing-key"`   // jwt签名
	ExpiresTime int64  `mapstructure:"expires-time" yaml:"expires-time"` // 过期时间
	Aud         string `mapstructure:"aud" yaml:"aud"`                   //接受者
	Issuer      string `mapstructure:"issuer" yaml:"issuer"`             // 签发者
}
