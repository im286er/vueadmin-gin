package config

// 短信配置
type Sms struct {
	AliSms
}

// 阿里云短信配置
type AliSms struct {
	AccessKeyId      string `mapstructure:"access-key-id" yaml:"access-key-id"`
	AccesskeySecrect string `mapstructure:"access-key-secret" yaml:"access-key-secret"`
	SignName         string `mapstructure:"sign-name" yaml:"sign-name"` // 签名
	TempCode         string `mapstructure:"tempcode" yaml:"tempcode"`   //短信模板
}
