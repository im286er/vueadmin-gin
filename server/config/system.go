package config

// 系统基本配置
type System struct {
	AccessLogStatus bool   `mapstructure:"access-log-status" yaml:"access-log-status"`
	Export          string `mapstructure:"export" yaml:"export"`
	Sso             bool   `mapstructure:"sso" yaml:"sso"`
}

// 获取免验证的方法
func (p System) GetNotAuthUri() []string {
	return []string{
		"/admin/Login/login",
		"/admin/Login/verify",
		"/admin/Index/logout",
		"/admin/Base/getSiteInfo",
		"/admin/Index/main",
		"/admin/Base/getShenshiqu",
		"/admin/Upload/upload",
		"/admin/Base/fileList",
		"/admin/Upload/createFile",
		"/admin/Base/clearCache",
		"/admin/Base/getRoleMenus",
	}
}
