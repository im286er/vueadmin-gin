package config

// 系统启动端口 运行模式配置
type Server struct {
	RunMode  string `mapstructure:"run-mode" yaml:"run-mode"`
	HttpPort int    `mapstructure:"http-port" yaml:"http-port"`
}
