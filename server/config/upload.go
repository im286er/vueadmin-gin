package config

// 上传文件配置
type Upload struct {
	QiniuOss
	AliyunOSS
	TencentCOS
	UploadPath      string `mapstructure:"upload-path" yaml:"upload-path"`
	Disks           string `mapstructure:"disks" yaml:"disks"`
	OssType         string `mapstructure:"oss-type" yaml:"oss-type"`
	AccessPath      string `mapstructure:"access-path" yaml:"access-path"`
	CheckFileStatus bool   `mapstructure:"check-file-status" yaml:"check-file-status"`
}

// 七牛云oss
type QiniuOss struct {
	Bucket          string `mapstructure:"bucket" yaml:"bucket"`
	AccessKey       string `mapstructure:"access-key" yaml:"access-key"`
	SecretKey       string `mapstructure:"secret-key" yaml:"secret-key"`
	Domain          string `mapstructure:"domain" yaml:"domain"`
	Zone            string `mapstructure:"zone" yaml:"zone"`
	UseHTTPS        bool   `mapstructure:"use-https" yaml:"use-https"`
	UseCdnDomains   bool   `mapstructure:"use-cdn-domains" yaml:"use-cdn-domains"`
	ClientUploadurl string `mapstructure:"client-uploadurl" yaml:"client-uploadurl"`
}

// 阿里云oss
type AliyunOSS struct {
	Endpoint        string `mapstructure:"endpoint" yaml:"endpoint"`
	AccessKeyId     string `mapstructure:"access-key-id" yaml:"access-key-id"`
	AccessKeySecret string `mapstructure:"access-key-secret" yaml:"access-key-secret"`
	BucketName      string `mapstructure:"bucket-name" yaml:"bucket-name"`
	Callback        string `mapstructure:"callback" yaml:"callback"`
}

// 腾讯云oss
type TencentCOS struct {
	Bucket    string `mapstructure:"bucket" yaml:"bucket"`
	Region    string `mapstructure:"region" yaml:"region"`
	SecretID  string `mapstructure:"secret-id" yaml:"secret-id"`
	SecretKey string `mapstructure:"secret-key" yaml:"secret-key"`
	Schema    string `mapstructure:"schema" yaml:"schema"`
}
