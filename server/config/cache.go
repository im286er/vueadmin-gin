package config

// 缓存配置
type Cache struct {
	Driver string `mapstructure:"driver" yaml:"driver"`
	Redis
}

// redis配置
type Redis struct {
	DB       int    `mapstructure:"db" yaml:"db"`
	Addr     string `mapstructure:"addr" yaml:"addr"`
	Password string `mapstructure:"password" yaml:"password"`
}
