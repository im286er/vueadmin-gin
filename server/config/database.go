package config

// 数据库配置
type Database struct {
	Db `structs:"db"` //默认mysql链接请不要动 struces标签 是连接名称
}

// 默认mysql配置
type Db struct {
	Type    string `mapstructure:"type" yaml:"type"`
	Host    string `mapstructure:"host" yaml:"host"`
	Port    string `mapstructure:"port" yaml:"port"`
	User    string `mapstructure:"user" yaml:"user"`
	Pwd     string `mapstructure:"pwd" yaml:"pwd"`
	Name    string `mapstructure:"name" yaml:"name"`
	Prefix  string `mapstructure:"prefix" yaml:"prefix"`
	Charset string `mapstructure:"charset" yaml:"charset"`
}

func (p Db) Dsn() string {
	return p.User + ":" + p.Pwd + "@tcp(" + p.Host + ":" + p.Port + ")/" + p.Name + "?charset=" + p.Charset
}
