package config

// 系统配置文件
type Config struct {
	Cache
	Database
	Server
	System
	Jwt
	Captcha
	Sms
	Upload
}
