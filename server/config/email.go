package config

// 邮箱配置
type Email struct {
	User string `mapstructure:"user" yaml:"user"`
	Pwd  string `mapstructure:"pwd" yaml:"pwd"`
	Smtp string `mapstructure:"smtp" yaml:"smtp"`
	Port int    `mapstructure:"port" yaml:"port"`
	Name string `mapstructure:"name" yaml:"name"`
}
