package conv

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"strings"
)

// 切片字符窜类型
type Strings []string

func (val Strings) Value() (driver.Value, error) {
	if len(val) > 0 {
		return strings.Join(val, ","), nil
	} else {
		return "", nil
	}
}

func (val *Strings) Scan(value interface{}) error {
	str, ok := value.([]byte)
	if !ok {
		return errors.New("不匹配的数据类型")
	}
	var slice []string
	for _, v := range strings.Split(string(str), ",") {
		slice = append(slice, v)
	}
	*val = slice
	return nil
}

// 省市区三级联动
type Ssq []string

func (ssq Ssq) Value() (driver.Value, error) {
	if len(ssq) > 0 {
		return strings.Join(ssq, "-"), nil
	} else {
		return "", nil
	}
}

func (ssq *Ssq) Scan(value interface{}) error {
	str, ok := value.([]byte)
	if !ok {
		return errors.New("不匹配的数据类型")
	}
	var slice []string
	for _, v := range strings.Split(string(str), "-") {
		slice = append(slice, v)
	}
	*ssq = slice
	return nil
}

// 键值对
type J struct {
	Key string `form:"key" json:"key"` //键名
	Val string `form:"val" json:"val"` //键值
}

type Jzd []*J

func (jzd Jzd) Value() (driver.Value, error) {
	if len(jzd) > 0 && String(jzd) != `[{"key":"","val":""}]` {
		str, err := json.Marshal(jzd)
		if err != nil {
			return nil, err
		}
		return string(str), nil
	} else {
		return "", nil
	}
}

func (jzd *Jzd) Scan(value interface{}) error {
	str, ok := value.([]byte)
	if !ok {
		return errors.New("不匹配的数据类型")
	}
	json.Unmarshal(str, &jzd)
	return nil
}

// 多文件
type file struct {
	Name string `form:"name" json:"name"` //文件名
	Url  string `form:"url" json:"url"`   //文件地址
}

type Files []*file

func (files Files) Value() (driver.Value, error) {
	if len(files) > 0 {
		str, err := json.Marshal(files)
		if err != nil {
			return nil, err
		}
		return string(str), nil
	} else {
		return "", nil
	}
}

func (files *Files) Scan(value interface{}) error {
	str, ok := value.([]byte)
	if !ok {
		return errors.New("不匹配的数据类型")
	}
	json.Unmarshal(str, &files)
	return nil
}
