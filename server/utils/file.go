package utils

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
	"github.com/xuri/excelize/v2"
	"io"
	"io/ioutil"
	"mime/multipart"
	"os"
	"path"
	"strings"
	"time"
)

// 判断目录是否存在
func PathExists(path string) (bool, error) {
	fi, err := os.Stat(path)
	if err == nil {
		if fi.IsDir() {
			return true, nil
		}
		return false, errors.New("存在同名文件")
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

// 判断文件是否存在
func FileExists(filepath string) bool {
	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		return false
	}
	return true
}

// 创建目录
func CreateDir(path string) error {
	if ok, _ := PathExists(path); !ok { // 判断是否有Director文件夹
		err := os.MkdirAll(path, os.ModePerm)
		return err
	}
	return nil
}

// 读取excel文件
func ReaderExcel(file *multipart.FileHeader) [][]string {
	fileHandle, err := file.Open()
	if err != nil {
		panic(err)
	}
	defer fileHandle.Close()

	f, err2 := excelize.OpenReader(fileHandle)
	if err2 != nil {
		panic(err2)
	}
	rows, _ := f.GetRows(f.GetSheetList()[0])
	return rows
}

// 判断文件是否存在  存在则OpenFile 不存在则Create
func OpenFile(filename string) (*os.File, error) {
	if FileExists(filename) {
		return os.Create(filename) //创建文件
	}
	return os.OpenFile(filename, os.O_APPEND, 0666) //打开文件
}

// 读取文件内容
func FileGetContents(filepath string) string {
	if FileExists(filepath) {
		content, err := ioutil.ReadFile(filepath)
		if err != nil {
			panic(err)
		}
		return conv.Bytes2String(content)
	} else {
		return ""
	}
}

// 写入文件内容  appned 是否追加内容 true追加
func FilePutContents(filepath string, content string, append ...bool) bool {
	if err := CreateDir(path.Dir(filepath)); err != nil {
		panic(err)
	}
	if len(append) > 0 {
		file, err := OpenFile(filepath)
		if err != nil {
			panic(err)
		}
		defer file.Close()
		if _, err2 := io.WriteString(file, content+"\n"); err != nil {
			panic(err2)
		}
	} else {
		if err := ioutil.WriteFile(filepath, conv.String2Bytes(content), 0666); err != nil {
			panic(err)
		}
	}

	return true
}

// 检查上传文件格式是否合法
func FileValid(fileExt string, typeString string) bool {
	ext := strings.Replace(fileExt, ".", "", -1)
	allowExt := conv.StringToSlice(strings.ToLower(typeString), ",")
	var valid = false
	if conv.IsValueInList(strings.ToLower(ext), allowExt) {
		valid = true
	}
	return valid
}

// 获取文件保存目录
func GetSavePath(c *gin.Context) string {
	applicationName := strings.Split(strings.TrimLeft(c.Request.RequestURI, "/"), "/")[0]
	savepath := fmt.Sprintf("/%s/%s/%s", global.CONF.Upload.UploadPath, applicationName, time.Now().Format("200601"))
	return savepath
}

// 创建上传文件存储目录
func CreatePath(savepath string) bool {
	if err := CreateDir(global.CONF.Upload.AccessPath + savepath); err != nil {
		panic(err)
	}
	return true
}

func GetEndPoint(aliOssEndpoint string) string {
	var endpoint string
	if strings.Contains(aliOssEndpoint, "aliyuncs.com") {
		if strings.Contains(aliOssEndpoint, "https") {
			endpoint = "https://" + global.CONF.AliyunOSS.BucketName + "." + aliOssEndpoint[8:]
		} else {
			endpoint = "http://" + global.CONF.AliyunOSS.BucketName + "." + aliOssEndpoint[7:]
		}
	} else {
		endpoint = aliOssEndpoint
	}
	return endpoint
}
