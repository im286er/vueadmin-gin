package request

// 列表请求公共参数
type PageReq struct {
	Page  int    `form:"page" json:"page"`   //页码
	Limit int    `form:"limit" json:"limit"` //每页显示条数
	Order string `form:"order" json:"order"` //排序方式
	Sort  string `form:"sort" json:"sort"`   //排序字段
}

// 图片验证码请求参数
type CaptchaReq struct {
	CaptchaId string `form:"captcha_id" json:"captcha_id" validate:"required" label:"验证id"`  //验证码id
	Captcha   string `form:"captcha" json:"captcha" validate:"required,captcha" label:"验证码"` //验证码
}

// 短信验证请求参数
type SmsReq struct {
	VerifyId string `form:"verify_id" json:"verify_id" validate:"required" label:"验证id"` //验证id
	Verify   string `form:"verify" json:"verify" validate:"required" label:"验证码"`        //验证码
	Mobile   string `form:"mobile" json:"mobile" validate:"required,mobile" label:"手机号"` //手机号
}
