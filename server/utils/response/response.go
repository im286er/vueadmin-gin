package response

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/go-sql-driver/mysql"
	"github.com/gookit/event"
	"go.mongodb.org/mongo-driver/mongo"
	"gorm.io/gorm"
	"net/http"
)

func OkResult(c *gin.Context, httpCode int, dataCode int, msg string, data interface{}) {
	c.JSON(httpCode, gin.H{
		"status": dataCode,
		"msg":    msg,
		"data":   data,
	})
}

func FailResult(c *gin.Context, httpCode int, dataCode int, msg string) {
	c.JSON(httpCode, gin.H{
		"status": dataCode,
		"msg":    msg,
	})
}

// 失败返回
func Fail(c *gin.Context, msg string) {
	FailResult(c, http.StatusOK, http.StatusPreconditionFailed, msg)
}

func FailWithCode(c *gin.Context, httpCode int, msg string) {
	FailResult(c, http.StatusOK, httpCode, msg)
}

// 成功返回
func Success(c *gin.Context, msg string, data interface{}) {
	OkResult(c, http.StatusOK, http.StatusOK, msg, data)
}

func Err(c *gin.Context, err error) {
	switch err.(type) {
	case *mysql.MySQLError:
		event.MustFire("exceptionLogEvent", event.M{
			"errMsg": err.(*mysql.MySQLError).Error(),
			"handle": c,
		})
		FailResult(c, http.StatusInternalServerError, http.StatusInternalServerError, "系统内部错误")
	default:
		if errors.Is(err, gorm.ErrRecordNotFound) {
			FailWithCode(c, http.StatusNotFound, err.Error())
		} else if errors.Is(err, mongo.ErrNoDocuments) {
			FailWithCode(c, http.StatusNotFound, err.Error())
		} else {
			FailWithCode(c, http.StatusPreconditionFailed, err.Error())
		}
	}

}
