package e

// 自定义验证器错误，带有状态码的
type ValidateWithCodeError struct {
	Msg  string
	Code int
}
