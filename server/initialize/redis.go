package initialize

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/vueadmin/config"
	"os"
)

// 获取redis链接
func GetRedisClient(redisInfo config.Redis) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     redisInfo.Addr,
		Password: redisInfo.Password,
		DB:       redisInfo.DB,
	})
	_, err := client.Ping(context.Background()).Result()
	if err != nil {
		fmt.Println("redis连接失败")
		os.Exit(-1)
	}
	return client
}
