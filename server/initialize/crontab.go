package initialize

import (
	"fmt"
	"github.com/vueadmin/global"
	"github.com/vueadmin/pkg/crontab"
	"os"
)

// 启动定时任务
func InitCrontab() {
	var list []*crontab.Crontab
	if err := global.DB.Where("status", 1).Find(&list).Error; err != nil {
		fmt.Println("拉取定时任务失败", err)
		os.Exit(-1)
	}
	for _, v := range list {
		if v.Type == 1 {
			crontab.Start(v.Target, v.Type, v.Rule, v.CrontabId)
		} else {
			if crontab.IsExistFunc(v.Target) {
				crontab.Start(v.Target, v.Type, v.Rule, v.CrontabId)
			}
		}
	}
}
