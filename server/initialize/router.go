package initialize

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/router"
	router2 "github.com/vueadmin/app/api/router"
	"github.com/vueadmin/global"
	"github.com/vueadmin/middleware"
	"reflect"
)

func Routers() *gin.Engine {
	var R = gin.New()

	R.Static("/assets", "./public/assets")
	R.Static("/uploads", "./public/uploads")

	R.Use(middleware.Cors(), middleware.GinRecovery())
	//访问日志中间件
	if global.CONF.System.AccessLogStatus {
		R.Use(middleware.AccessLog())
	}
	//R.Use(middleware.DoLogLog())
	R.GET("/health", func(c *gin.Context) {
		c.JSON(200, "ok")
	})

	//注册admin路由
	for _, ctrl := range router.AdminRouter {
		router := reflect.ValueOf(ctrl).MethodByName("Router")
		callback := router.Interface().(func(r *gin.RouterGroup))
		callback(R.Group(""))
	}

	//注册api路由
	for _, ctrl := range router2.ApiRouter {
		router := reflect.ValueOf(ctrl).MethodByName("Router")
		callback := router.Interface().(func(r *gin.RouterGroup))
		callback(R.Group(""))
	}

	return R
}
