package global

import (
	"github.com/go-redis/redis/v8"
	"github.com/vueadmin/config"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

var (
	DB        *gorm.DB           //默认mysql实例
	CONF      config.Config      //默认配置实例
	LOG       *zap.SugaredLogger //默认日志实例
	ACCESSLOG *zap.SugaredLogger //访问日志实例
	REDIS     *redis.Client      //redis链接实例
)
