package service

import (
	"github.com/vueadmin/app/api/dto"
	"github.com/vueadmin/app/api/model"
	"github.com/vueadmin/app/api/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

var LinkService = new(link)

type link struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p link) GetPageList(req *dto.LinkPageReq) ([]*vo.LinkList, int64, error) {
	var (
		entity model.Link
		list   []*vo.LinkList
		count  int64
	)
	query := global.DB.Model(&entity)
	if !conv.IsEmpty(req.LinkId) {
		query = query.Where("link_id = ?", req.LinkId)
	}
	if !conv.IsEmpty(req.Title) {
		query = query.Where("title = ?", req.Title)
	}
	if !conv.IsEmpty(req.LinkcataId) {
		query = query.Where("linkcata_id = ?", req.LinkcataId)
	}
	if !conv.IsEmpty(req.Status) {
		query = query.Where("status = ?", req.Status)
	}
	if !conv.IsEmpty(req.CreateTime) {
		query = query.Where("create_time BETWEEN ? AND ?", conv.UnixTime(req.CreateTime[0]), conv.UnixTime(req.CreateTime[1]))
	}
	order := "sortid asc"
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)

	return list, count, nil
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (p link) Add(req *dto.LinkAddReq) (uint, error) {
	entity := model.Link{}
	entity.Title = req.Title
	entity.Url = req.Url
	entity.LinkcataId = req.LinkcataId
	entity.Logo = req.Logo
	entity.Status = req.Status
	entity.Sortid = req.Sortid
	entity.CreateTime = conv.NowUnix()

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	if entity.Sortid == 0 {
		if err := global.DB.Model(&entity).Update("sortid", entity.LinkId).Error; err != nil {
			return 0, err
		}
	}
	return entity.LinkId, nil
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (p link) Update(req *dto.LinkUpdateReq) error {
	entity := model.Link{
		LinkId: req.LinkId,
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	entity.Title = req.Title
	entity.Url = req.Url
	entity.LinkcataId = req.LinkcataId
	entity.Logo = req.Logo
	entity.Status = req.Status
	entity.Sortid = req.Sortid
	entity.CreateTime = conv.UnixTime(req.CreateTime)

	field := conv.StringToSlice("title,url,linkcata_id,logo,status,sortid,create_time", ",")

	if err := global.DB.Select(field).Updates(&entity).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (p link) Delete(req *dto.LinkDeleteReq) error {
	query := global.DB.Where("link_id IN ?", conv.Slice(req.LinkId))
	if err := query.Delete(&model.Link{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p link) Detail(req *dto.LinkDetailReq) (*vo.LinkDetail, error) {
	var info vo.LinkDetail
	query := global.DB.Model(&model.Link{})
	if err := query.Where("link_id = ?", req.LinkId).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}
