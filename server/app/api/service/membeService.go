package service

import (
	"github.com/vueadmin/app/api/dto"
	"github.com/vueadmin/app/api/model"
	"github.com/vueadmin/app/api/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

var MembeService = new(membe)

type membe struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p membe) GetPageList(req *dto.MembePageReq) ([]*vo.MembeList, int64, error) {
	var (
		entity model.Membe
		list   []*vo.MembeList
		count  int64
	)
	query := global.DB.Model(&entity)
	if !conv.IsEmpty(req.MembeId) {
		query = query.Where("membe_id = ?", req.MembeId)
	}
	if !conv.IsEmpty(req.Username) {
		query = query.Where("username = ?", req.Username)
	}
	if !conv.IsEmpty(req.Sex) {
		query = query.Where("sex = ?", req.Sex)
	}
	if !conv.IsEmpty(req.Mobile) {
		query = query.Where("mobile = ?", req.Mobile)
	}
	if !conv.IsEmpty(req.Email) {
		query = query.Where("email = ?", req.Email)
	}
	if !conv.IsEmpty(req.Status) {
		query = query.Where("status = ?", req.Status)
	}
	if !conv.IsEmpty(req.Ssq) {
		query = query.Where("ssq like ?", "%"+conv.Join(req.Ssq, "-")+"%")
	}
	if !conv.IsEmpty(req.CreateTime) {
		query = query.Where("create_time BETWEEN ? AND ?", conv.UnixTime(req.CreateTime[0]), conv.UnixTime(req.CreateTime[1]))
	}
	order := "membe_id desc"
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)

	return list, count, nil
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (p membe) Add(req *dto.MembeAddReq) (uint, error) {
	entity := model.Membe{}
	entity.Username = req.Username
	entity.Sex = req.Sex
	entity.Mobile = req.Mobile
	entity.Pic = req.Pic
	entity.Email = req.Email
	entity.Password = conv.EncryptPassword(req.Password)
	entity.Amount = req.Amount
	entity.Status = req.Status
	entity.Ssq = req.Ssq
	entity.CreateTime = conv.NowUnix()

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	return entity.MembeId, nil
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (p membe) Update(req *dto.MembeUpdateReq) error {
	entity := model.Membe{
		MembeId: req.MembeId,
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	entity.Username = req.Username
	entity.Sex = req.Sex
	entity.Mobile = req.Mobile
	entity.Pic = req.Pic
	entity.Email = req.Email
	entity.Amount = req.Amount
	entity.Status = req.Status
	entity.Ssq = req.Ssq
	entity.CreateTime = conv.UnixTime(req.CreateTime)

	field := conv.StringToSlice("username,sex,mobile,pic,email,amount,status,ssq,create_time", ",")

	if err := global.DB.Select(field).Updates(&entity).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (p membe) Delete(req *dto.MembeDeleteReq) error {
	query := global.DB.Where("membe_id IN ?", conv.Slice(req.MembeId))
	if err := query.Delete(&model.Membe{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p membe) Detail(req *dto.MembeDetailReq) (*vo.MembeDetail, error) {
	var info vo.MembeDetail
	query := global.DB.Model(&model.Membe{})
	if err := query.Where("membe_id = ?", req.MembeId).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}

/**
 * @description(重置密码)
 * @buildcode(true)
 */
func (membe) ResetPwd(req *dto.MembeResetPwdReq) error {
	entity := model.Membe{
		MembeId: conv.Uint(req.MembeId),
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	if err := global.DB.Model(&entity).Update("password", conv.EncryptPassword(req.Password)).Error; err != nil {
		return err
	}
	return nil
}
