package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/api/controller"
	"github.com/vueadmin/middleware"
)

type Membe struct{}

/**
 * @description(会员管理路由)
 * @buildcode(true)
 */
func (Membe) Router(r *gin.RouterGroup) {
	r.POST("/api/Membe/index", controller.Membe.GetPageList)                                                    //数据列表
	r.POST("/api/Membe/add", middleware.JwtVerify("api"), controller.Membe.Add)                                 //添加
	r.POST("/api/Membe/update", middleware.JwtVerify("api"), controller.Membe.Update)                           //修改
	r.POST("/api/Membe/delete", middleware.JwtVerify("api"), controller.Membe.Delete)                           //删除
	r.POST("/api/Membe/detail", controller.Membe.Detail)                                                        //查看详情
	r.POST("/api/Membe/resetPwd", middleware.SmsAuth(), middleware.JwtVerify("api"), controller.Membe.ResetPwd) //重置密码
}
