package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/api/controller"
	"github.com/vueadmin/middleware"
)

type Link struct{}

/**
 * @description(友情链接路由)
 * @buildcode(true)
 */
func (Link) Router(r *gin.RouterGroup) {
	r.POST("/api/Link/index", controller.Link.GetPageList)                                                //数据列表
	r.POST("/api/Link/add", middleware.SmsAuth(), middleware.JwtVerify("api"), controller.Link.Add)       //添加
	r.POST("/api/Link/update", middleware.SmsAuth(), middleware.JwtVerify("api"), controller.Link.Update) //修改
	r.POST("/api/Link/delete", middleware.SmsAuth(), middleware.JwtVerify("api"), controller.Link.Delete) //删除
	r.POST("/api/Link/detail", controller.Link.Detail)                                                    //查看详情
}
