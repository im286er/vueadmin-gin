package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/api/dto"
	"github.com/vueadmin/app/api/service"
	"github.com/vueadmin/utils"
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/common/result"
	"github.com/vueadmin/utils/response"
)

var Membe = new(membe)

type membe struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (membe) GetPageList(c *gin.Context) {
	req := dto.MembePageReq{
		PageReq: request.PageReq{Page: 1, Limit: 20},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, count, err := service.MembeService.GetPageList(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", result.M{
		"data":  list,
		"total": count,
	})
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (membe) Add(c *gin.Context) {
	req := dto.MembeAddReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	ret, err := service.MembeService.Add(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", ret)
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (membe) Update(c *gin.Context) {
	req := dto.MembeUpdateReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.MembeService.Update(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (membe) Delete(c *gin.Context) {
	req := dto.MembeDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.MembeService.Delete(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "删除成功", nil)
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (membe) Detail(c *gin.Context) {
	req := dto.MembeDetailReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.MembeService.Detail(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(重置密码)
 * @buildcode(true)
 */
func (membe) ResetPwd(c *gin.Context) {
	req := dto.MembeResetPwdReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.MembeService.ResetPwd(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}
