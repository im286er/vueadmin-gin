package model

import (
	"fmt"
	"github.com/vueadmin/global"
	"github.com/vueadmin/pkg/cache"
	"gorm.io/gorm"
)

/**
 * @description(友情链接模型)
 * @buildcode(true)
 */
type Link struct {
	LinkId     uint   `gorm:"primaryKey" json:"link_id"` //编号
	Title      string `json:"title"`                     //链接名称
	Url        string `json:"url"`                       //链接地址
	LinkcataId int    `json:"linkcata_id"`               //所属分类
	Logo       string `json:"logo"`                      //logo
	Status     int8   `json:"status"`                    //状态
	Sortid     int    `json:"sortid"`                    //排序
	CreateTime int64  `json:"create_time"`               //创建时间
}

/**
 * @description(友情链接数据表)
 * @buildcode(true)
 */
func (Link) TableName() string {
	return global.CONF.Db.Prefix + "link"
}

/**
 * @description(添加更新缓存)
 * @buildcode(true)
 */
func (p *Link) AfterAdd(tx *gorm.DB) (err error) {
	cache.New().DeleteByLike("link:list:*")
	return nil
}

/**
 * @description(修改更新缓存)
 * @buildcode(true)
 */
func (p *Link) AfterUpdate(tx *gorm.DB) (err error) {
	cache.New().Delete(fmt.Sprintf("link:%d", p.LinkId))
	cache.New().DeleteByLike("link:list:*")
	return nil
}

/**
 * @description(删除更新缓存)
 * @buildcode(true)
 */
func (p *Link) AfterDelete(tx *gorm.DB) (err error) {
	cache.New().DeleteByLike("link:list:*")
	return nil
}
