package dto

import (
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/conv"
)

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type MembePageReq struct {
	request.PageReq
	MembeId    *uint    `form:"membe_id" json:"membe_id"`       //主键id
	Username   string   `form:"username" json:"username"`       //用户名
	Sex        int      `form:"sex" json:"sex"`                 //性别
	Mobile     string   `form:"mobile" json:"mobile"`           //手机号
	Email      string   `form:"email" json:"email"`             //邮箱
	Status     *int8    `form:"status" json:"status"`           //状态
	Ssq        []string `form:"ssq" json:"ssq"`                 //省市区
	CreateTime []string `form:"create_time" json:"create_time"` //创建时间
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type MembeAddReq struct {
	Username   string   `form:"username" json:"username" validate:"required" label:"用户名"`    //用户名
	Sex        int      `form:"sex" json:"sex" validate:"required" label:"性别"`               //性别
	Mobile     string   `form:"mobile" json:"mobile" validate:"mobile,required" label:"手机号"` //手机号
	Pic        string   `form:"pic" json:"pic" `                                             //头像
	Email      string   `form:"email" json:"email" validate:"required,email" label:"邮箱"`     //邮箱
	Password   string   `form:"password" json:"password" `                                   //密码
	Amount     float64  `form:"amount" json:"amount" `                                       //积分
	Status     int8     `form:"status" json:"status" `                                       //状态
	Ssq        conv.Ssq `form:"ssq" json:"ssq" `                                             //省市区
	CreateTime string   `form:"create_time" json:"create_time" `                             //创建时间
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type MembeUpdateReq struct {
	MembeId    uint     `form:"membe_id" json:"membe_id" validate:"required" label:"编号"`     //主键id
	Username   string   `form:"username" json:"username" validate:"required" label:"用户名"`    //用户名
	Sex        int      `form:"sex" json:"sex" validate:"required" label:"性别"`               //性别
	Mobile     string   `form:"mobile" json:"mobile" validate:"mobile,required" label:"手机号"` //手机号
	Pic        string   `form:"pic" json:"pic" `                                             //头像
	Email      string   `form:"email" json:"email" validate:"required,email" label:"邮箱"`     //邮箱
	Amount     float64  `form:"amount" json:"amount" `                                       //积分
	Status     int8     `form:"status" json:"status" `                                       //状态
	Ssq        conv.Ssq `form:"ssq" json:"ssq" `                                             //省市区
	CreateTime string   `form:"create_time" json:"create_time" `                             //创建时间
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type MembeDeleteReq struct {
	MembeId uint `form:"membe_id" json:"membe_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type MembeDetailReq struct {
	MembeId uint `form:"membe_id" json:"membe_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(重置密码)
 * @buildcode(true)
 */
type MembeResetPwdReq struct {
	MembeId  uint   `form:"membe_id" json:"membe_id" validate:"required" label:"编号"` //主键id
	Password string `form:"password" json:"password"`                                //密码
}
