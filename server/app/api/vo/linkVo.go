package vo

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type LinkList struct {
	LinkId     uint   `json:"link_id"`     //主键id
	Title      string `json:"title"`       //链接名称
	Url        string `json:"url"`         //链接地址
	Logo       string `json:"logo"`        //logo
	Status     int8   `json:"status"`      //状态
	Sortid     int    `json:"sortid"`      //排序
	CreateTime int64  `json:"create_time"` //创建时间
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type LinkDetail struct {
	LinkId     uint   `json:"link_id"`     //主键id
	Title      string `json:"title"`       //链接名称
	Url        string `json:"url"`         //链接地址
	Logo       string `json:"logo"`        //logo
	Status     int8   `json:"status"`      //状态
	Sortid     int    `json:"sortid"`      //排序
	CreateTime int64  `json:"create_time"` //创建时间
}
