package vo

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminMembeList struct {
	MembeId    uint    `json:"membe_id"`    //主键id
	Username   string  `json:"username"`    //用户名
	Sex        int     `json:"sex"`         //性别
	Mobile     string  `json:"mobile"`      //手机号
	Pic        string  `json:"pic"`         //头像
	Email      string  `json:"email"`       //邮箱
	Amount     float64 `json:"amount"`      //积分
	Status     int8    `json:"status"`      //状态
	Ssq        string  `json:"ssq"`         //省市区
	CreateTime int64   `json:"create_time"` //创建时间
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminMembeDetail struct {
	MembeId    uint    `json:"membe_id"`    //主键id
	Username   string  `json:"username"`    //用户名
	Sex        int     `json:"sex"`         //性别
	Mobile     string  `json:"mobile"`      //手机号
	Pic        string  `json:"pic"`         //头像
	Email      string  `json:"email"`       //邮箱
	Amount     float64 `json:"amount"`      //积分
	Status     int8    `json:"status"`      //状态
	Ssq        string  `json:"ssq"`         //省市区
	CreateTime int64   `json:"create_time"` //创建时间
}

/**
 * @description(客户端导出)
 * @buildcode(true)
 */
type AdminMembeExport struct {
	Username   string `json:"username"`    //用户名
	Sex        string `json:"sex"`         //性别
	Mobile     string `json:"mobile"`      //手机号
	Pic        string `json:"pic"`         //头像
	Email      string `json:"email"`       //邮箱
	Password   string `json:"password"`    //密码
	Amount     string `json:"amount"`      //积分
	Status     string `json:"status"`      //状态
	Ssq        string `json:"ssq"`         //省市区
	CreateTime string `json:"create_time"` //创建时间
}

/**
 * @description(服务端导出)
 * @buildcode(true)
 */
type AdminMembeExportServer struct {
	Username   string `json:"username"`    //用户名
	Sex        string `json:"sex"`         //性别
	Mobile     string `json:"mobile"`      //手机号
	Pic        string `json:"pic"`         //头像
	Email      string `json:"email"`       //邮箱
	Password   string `json:"password"`    //密码
	Amount     string `json:"amount"`      //积分
	Status     string `json:"status"`      //状态
	Ssq        string `json:"ssq"`         //省市区
	CreateTime string `json:"create_time"` //创建时间
}
