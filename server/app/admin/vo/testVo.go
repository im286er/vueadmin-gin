package vo

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminTestList struct {
	TestId    uint   `json:"test_id"`    //主键id
	Title     string `json:"title"`      //标题
	Status    int8   `json:"status"`     //状态
	DeletedAt string `json:"deleted_at"` //软删除标记
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminTestDetail struct {
	TestId uint   `json:"test_id"` //主键id
	Title  string `json:"title"`   //标题
	Status int8   `json:"status"`  //状态
}
