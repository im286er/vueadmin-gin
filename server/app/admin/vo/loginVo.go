package vo

import "github.com/vueadmin/app/admin/model"

// 后台登录返回
type AdminLogin struct {
	UserId uint             `json:"user_id"`                        //用户ID
	User   string           `json:"user"`                           //用户名
	RoleId uint             `json:"role_id"`                        //角色id
	Status int8             `json:"status"`                         //状态
	Pwd    string           `json:"pwd"`                            //登录密码
	Role   *model.AdminRole `json:"role" gorm:"foreignkey:role_id"` //关联模型
	Access string           `json:"access" gorm:"-"`                //权限节点
}
