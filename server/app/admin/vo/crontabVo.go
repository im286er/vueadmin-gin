package vo

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminCrontabList struct {
	CrontabId   uint   `json:"crontab_id"`    //主键id
	Name        string `json:"name"`          //任务名称
	Type        int    `json:"type"`          //任务类型
	Rule        string `json:"rule"`          //规则
	Status      int8   `json:"status"`        //状态
	Target      string `json:"target"`        //调用目标
	LastRunTime int64  `json:"last_run_time"` //最后执行时间
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminCrontabDetail struct {
	CrontabId   uint   `json:"crontab_id"`    //主键id
	Name        string `json:"name"`          //任务名称
	Type        int    `json:"type"`          //任务类型
	Rule        string `json:"rule"`          //规则
	Status      int8   `json:"status"`        //状态
	Target      string `json:"target"`        //调用目标
	LastRunTime int64  `json:"last_run_time"` //最后执行时间
}
