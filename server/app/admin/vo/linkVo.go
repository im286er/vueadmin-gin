package vo

import "github.com/vueadmin/app/admin/model"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminLinkList struct {
	LinkId     uint                 `json:"link_id"`                                //主键id
	Title      string               `json:"title"`                                  //链接名称
	Url        string               `json:"url"`                                    //链接地址
	Logo       string               `json:"logo"`                                   //logo
	Status     int8                 `json:"status"`                                 //状态
	Sortid     int                  `json:"sortid"`                                 //排序
	CreateTime int64                `json:"create_time"`                            //创建时间
	LinkcataId int                  `json:"linkcata_id"`                            //所属分类
	Linkcata   *model.AdminLinkcata `json:"linkcata" gorm:"foreignkey:linkcata_id"` //关联模型
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminLinkDetail struct {
	LinkId     uint                 `json:"link_id"`                                //主键id
	Title      string               `json:"title"`                                  //链接名称
	Url        string               `json:"url"`                                    //链接地址
	Logo       string               `json:"logo"`                                   //logo
	Status     int8                 `json:"status"`                                 //状态
	Sortid     int                  `json:"sortid"`                                 //排序
	CreateTime int64                `json:"create_time"`                            //创建时间
	LinkcataId int                  `json:"linkcata_id"`                            //所属分类
	Linkcata   *model.AdminLinkcata `json:"linkcata" gorm:"foreignkey:linkcata_id"` //关联模型
}

/**
 * @description(查询所属分类列表)
 * @buildcode(true)
 */
type AdminLinkLinkcataId struct {
	LinkcataId int    `json:"val"`
	ClassName  string `json:"key"`
}
