package vo

import "github.com/vueadmin/app/admin/model"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminAdminuserList struct {
	UserId     uint             `json:"user_id"`                        //主键id
	Name       string           `json:"name"`                           //用户姓名
	User       string           `json:"user"`                           //用户名
	Note       string           `json:"note"`                           //备注
	Status     int8             `json:"status"`                         //状态
	CreateTime int64            `json:"create_time"`                    //创建时间
	RoleId     int              `json:"role_id"`                        //所属角色
	Role       *model.AdminRole `json:"role" gorm:"foreignkey:role_id"` //关联模型
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminAdminuserDetail struct {
	UserId     uint   `json:"user_id"`     //主键id
	Name       string `json:"name"`        //用户姓名
	User       string `json:"user"`        //用户名
	Note       string `json:"note"`        //备注
	Status     int8   `json:"status"`      //状态
	CreateTime int64  `json:"create_time"` //创建时间
}

/**
 * @description(查询所属角色列表)
 * @buildcode(true)
 */
type AdminAdminuserRoleId struct {
	RoleId int    `json:"val"`
	Name   string `json:"key"`
}
