package vo

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminSupplierList struct {
	SupplierId   uint   `json:"supplier_id"`   //主键id
	SupplierName string `json:"supplier_name"` //供应商名称
	Status       int8   `json:"status"`        //状态
	Username     string `json:"username"`      //用户名
	CreateTime   int64  `json:"create_time"`   //创建时间
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminSupplierDetail struct {
	SupplierId   uint   `json:"supplier_id"`   //主键id
	SupplierName string `json:"supplier_name"` //供应商名称
	Status       int8   `json:"status"`        //状态
	Username     string `json:"username"`      //用户名
	CreateTime   int64  `json:"create_time"`   //创建时间
}
