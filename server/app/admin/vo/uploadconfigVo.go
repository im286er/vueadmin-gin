package vo

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminUploadconfigList struct {
	Id          uint   `json:"id"`           //主键id
	Title       string `json:"title"`        //配置名称
	ThumbStatus int8   `json:"thumb_status"` //生成缩略图
	ThumbWidth  string `json:"thumb_width"`  //缩略图宽
	ThumbHeight string `json:"thumb_height"` //缩略图高
	ThumbType   int    `json:"thumb_type"`   //缩放类型
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminUploadconfigDetail struct {
	Id          uint   `json:"id"`           //主键id
	Title       string `json:"title"`        //配置名称
	ThumbStatus int8   `json:"thumb_status"` //生成缩略图
	ThumbWidth  string `json:"thumb_width"`  //缩略图宽
	ThumbHeight string `json:"thumb_height"` //缩略图高
	ThumbType   int    `json:"thumb_type"`   //缩放类型
}
