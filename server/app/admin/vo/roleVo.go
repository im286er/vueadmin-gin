package vo

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminRoleList struct {
	RoleId      uint   `json:"role_id"`     //主键id
	Name        string `json:"name"`        //角色名称
	Status      int8   `json:"status"`      //状态
	Description string `json:"description"` //描述
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminRoleDetail struct {
	RoleId      uint   `json:"role_id"`     //主键id
	Name        string `json:"name"`        //角色名称
	Status      int8   `json:"status"`      //状态
	Description string `json:"description"` //描述
}
