package vo

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminCarList struct {
	CarId      uint   `json:"car_id"`      //主键id
	MembeId    int    `json:"membe_id"`    //用户id
	CarNo      string `json:"car_no"`      //车牌号
	CreateTime int64  `json:"create_time"` //创建时间
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminCarDetail struct {
	CarId      uint   `json:"car_id"`      //主键id
	MembeId    int    `json:"membe_id"`    //用户id
	CarNo      string `json:"car_no"`      //车牌号
	CreateTime int64  `json:"create_time"` //创建时间
}
