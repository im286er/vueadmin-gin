package vo

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminLogList struct {
	Id              uint   `json:"id"`               //主键id
	ApplicationName string `json:"application_name"` //所属应用
	Type            int    `json:"type"`             //日志类型
	Username        string `json:"username"`         //用户名
	Url             string `json:"url"`              //请求url
	Ip              string `json:"ip"`               //客户端ip
	CreateTime      int64  `json:"create_time"`      //创建时间
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminLogDetail struct {
	Id              uint   `json:"id"`               //主键id
	ApplicationName string `json:"application_name"` //所属应用
	Type            int    `json:"type"`             //日志类型
	Username        string `json:"username"`         //用户名
	Url             string `json:"url"`              //请求url
	Ip              string `json:"ip"`               //客户端ip
	CreateTime      int64  `json:"create_time"`      //创建时间
}

/**
 * @description(导出)
 * @buildcode(true)
 */
type AdminLogExport struct {
	ApplicationName string `json:"application_name"` //所属应用
	Type            string `json:"type"`             //日志类型
	Username        string `json:"username"`         //用户名
	Url             string `json:"url"`              //请求url
	Ip              string `json:"ip"`               //客户端ip
	Useragent       string `json:"useragent"`        //浏览器信息
	Content         string `json:"content"`          //请求内容
	Errmsg          string `json:"errmsg"`           //异常信息
	CreateTime      string `json:"create_time"`      //创建时间
}

/**
 * @description(查询所属应用列表)
 * @buildcode(true)
 */
type AdminLogApplicationName struct {
	AppDir          string `json:"val"`
	ApplicationName string `json:"key"`
}
