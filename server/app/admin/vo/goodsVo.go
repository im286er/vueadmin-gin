package vo

import "github.com/vueadmin/app/admin/model"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminGoodsList struct {
	GoodsId    uint                  `json:"goods_id"`                               //主键id
	GoodsName  string                `json:"goods_name"`                             //商品名称
	Pic        string                `json:"pic"`                                    //封面图
	SalePrice  float64               `json:"sale_price"`                             //销售价
	Status     int8                  `json:"status"`                                 //状态
	Sortid     int                   `json:"sortid"`                                 //排序
	CreateTime int64                 `json:"create_time"`                            //发布时间
	ClassId    int                   `json:"class_id"`                               //所属分类
	SupplierId int                   `json:"supplier_id"`                            //供应商
	GoodsCata  *model.AdminGoodsCata `json:"goodscata" gorm:"foreignkey:class_id"`   //关联模型
	Supplier   *model.AdminSupplier  `json:"supplier" gorm:"foreignkey:supplier_id"` //关联模型
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminGoodsDetail struct {
	GoodsId    uint                  `json:"goods_id"`                               //主键id
	GoodsName  string                `json:"goods_name"`                             //商品名称
	Pic        string                `json:"pic"`                                    //封面图
	SalePrice  float64               `json:"sale_price"`                             //销售价
	Status     int8                  `json:"status"`                                 //状态
	Sortid     int                   `json:"sortid"`                                 //排序
	CreateTime int64                 `json:"create_time"`                            //发布时间
	ClassId    int                   `json:"class_id"`                               //所属分类
	SupplierId int                   `json:"supplier_id"`                            //供应商
	GoodsCata  *model.AdminGoodsCata `json:"goodscata" gorm:"foreignkey:class_id"`   //关联模型
	Supplier   *model.AdminSupplier  `json:"supplier" gorm:"foreignkey:supplier_id"` //关联模型
}

/**
 * @description(查询供应商列表)
 * @buildcode(true)
 */
type AdminGoodsSupplierId struct {
	SupplierId   int    `json:"val"`
	SupplierName string `json:"key"`
}

/**
 * @description(查询所属分类列表)
 * @buildcode(true)
 */
type AdminGoodsClassId struct {
	ClassId   int                  `json:"val"`
	ClassName string               `json:"key"`
	Pid       int                  `json:"pid"`
	Children  []*AdminGoodsClassId `json:"children" gorm:"-"`
}
