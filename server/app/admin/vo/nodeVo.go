package vo

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminNodeList struct {
	NodeId        uint             `json:"node_id"`           //主键id
	Title         string           `json:"title"`             //节点名称
	Type          int              `json:"type"`              //类型
	ComponentPath string           `json:"component_path"`    //组件路径
	Status        int8             `json:"status"`            //状态
	Icon          string           `json:"icon"`              //图标
	Sortid        int              `json:"sortid"`            //排序
	Path          string           `json:"path"`              //节点标识
	Pid           int              `json:"pid"`               //父级分类
	Children      []*AdminNodeList `json:"children" gorm:"-"` //下级列表
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminNodeDetail struct {
	NodeId        uint   `json:"node_id"`        //主键id
	Title         string `json:"title"`          //节点名称
	Type          int    `json:"type"`           //类型
	ComponentPath string `json:"component_path"` //组件路径
	Status        int8   `json:"status"`         //状态
	Icon          string `json:"icon"`           //图标
	Sortid        int    `json:"sortid"`         //排序
	Path          string `json:"path"`           //节点标识
}

/**
 * @description(查询所属父类列表)
 * @buildcode(true)
 */
type AdminNodePid struct {
	NodeId   int             `json:"val"`
	Title    string          `json:"key"`
	Pid      int             `json:"pid"`
	Children []*AdminNodePid `json:"children" gorm:"-"`
}
