package vo

import "github.com/vueadmin/app/admin/model"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminGoodsCataList struct {
	ClassId    uint                  `json:"class_id"`                               //主键id
	ClassName  string                `json:"class_name"`                             //分类名称
	Status     int8                  `json:"status"`                                 //状态
	Sortid     int                   `json:"sortid"`                                 //排序
	Pid        int                   `json:"pid"`                                    //父级分类
	Children   []*AdminGoodsCataList `json:"children" gorm:"-"`                      //下级列表
	SupplierId int                   `json:"supplier_id"`                            //供应商
	Supplier   *model.AdminSupplier  `json:"supplier" gorm:"foreignkey:supplier_id"` //关联模型
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminGoodsCataDetail struct {
	ClassId   uint   `json:"class_id"`   //主键id
	ClassName string `json:"class_name"` //分类名称
	Status    int8   `json:"status"`     //状态
	Sortid    int    `json:"sortid"`     //排序
}

/**
 * @description(查询供应商列表)
 * @buildcode(true)
 */
type AdminGoodsCataSupplierId struct {
	SupplierId   int    `json:"val"`
	SupplierName string `json:"key"`
}

/**
 * @description(查询所属父类列表)
 * @buildcode(true)
 */
type AdminGoodsCataPid struct {
	ClassId   int                  `json:"val"`
	ClassName string               `json:"key"`
	Pid       int                  `json:"pid"`
	Children  []*AdminGoodsCataPid `json:"children" gorm:"-"`
}
