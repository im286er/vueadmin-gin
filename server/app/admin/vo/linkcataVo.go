package vo

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminLinkcataList struct {
	LinkcataId uint   `json:"linkcata_id"` //主键id
	ClassName  string `json:"class_name"`  //分类名称
	Status     int8   `json:"status"`      //状态
	Jdt        int    `json:"jdt"`         //进度条
	Count      string `json:"count"`       //分类下数量
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminLinkcataDetail struct {
	LinkcataId uint   `json:"linkcata_id"` //主键id
	ClassName  string `json:"class_name"`  //分类名称
	Status     int8   `json:"status"`      //状态
	Jdt        int    `json:"jdt"`         //进度条
	Count      string `json:"count"`       //分类下数量
}
