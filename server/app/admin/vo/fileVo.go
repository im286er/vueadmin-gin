package vo

// 数据列表
type AdminFileList struct {
	Id         uint   `json:"id"`          //主键id
	Filepath   string `json:"filepath"`    //图片路径
	Hash       string `json:"hash"`        //文件hash值
	CreateTime int    `json:"create_time"` //创建时间
	Disk       string `json:"disk"`        //存储方式
	Type       int8   `json:"type"`        //文件类型
}
