package hook

import (
	"errors"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/utils/conv"
)

// 删除用户前置钩子
func BeforAdminAdminuserDeleteHook(req *dto.AdminAdminuserDeleteReq) error {
	ids := conv.Slice(req.UserId)
	if len(ids) > 1 {
		return errors.New("禁止批量删除")
	}
	if conv.Int(req.UserId) == 1 {
		return errors.New("超级管理员禁止删除")
	}
	return nil
}
