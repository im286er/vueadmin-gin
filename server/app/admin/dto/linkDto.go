package dto

import "github.com/vueadmin/utils/common/request"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminLinkPageReq struct {
	request.PageReq
	LinkId     *uint    `form:"link_id" json:"link_id"`         //主键id
	Title      string   `form:"title" json:"title"`             //链接名称
	LinkcataId int      `form:"linkcata_id" json:"linkcata_id"` //所属分类
	Status     *int8    `form:"status" json:"status"`           //状态
	CreateTime []string `form:"create_time" json:"create_time"` //创建时间
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminLinkAddReq struct {
	Title      string `form:"title" json:"title" `             //链接名称
	Url        string `form:"url" json:"url" `                 //链接地址
	LinkcataId int    `form:"linkcata_id" json:"linkcata_id" ` //所属分类
	Logo       string `form:"logo" json:"logo" `               //logo
	Status     int8   `form:"status" json:"status" `           //状态
	Sortid     int    `form:"sortid" json:"sortid" `           //排序
	CreateTime string `form:"create_time" json:"create_time" ` //创建时间
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminLinkUpdateReq struct {
	LinkId     uint   `form:"link_id" json:"link_id" validate:"required" label:"编号"` //主键id
	Title      string `form:"title" json:"title" `                                   //链接名称
	Url        string `form:"url" json:"url" `                                       //链接地址
	LinkcataId int    `form:"linkcata_id" json:"linkcata_id" `                       //所属分类
	Logo       string `form:"logo" json:"logo" `                                     //logo
	Status     int8   `form:"status" json:"status" `                                 //状态
	Sortid     int    `form:"sortid" json:"sortid" `                                 //排序
	CreateTime string `form:"create_time" json:"create_time" `                       //创建时间
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminLinkGetUpdateInfoReq struct {
	LinkId interface{} `form:"link_id" json:"link_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminLinkDeleteReq struct {
	LinkId interface{} `form:"link_id" json:"link_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminLinkDetailReq struct {
	LinkId interface{} `form:"link_id" json:"link_id" validate:"required" label:"编号"` //主键id
}
