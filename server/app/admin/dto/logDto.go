package dto

import "github.com/vueadmin/utils/common/request"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminLogPageReq struct {
	request.PageReq
	Id              *uint    `form:"id" json:"id"`                             //主键id
	ApplicationName string   `form:"application_name" json:"application_name"` //所属应用
	Type            int      `form:"type" json:"type"`                         //日志类型
	Username        string   `form:"username" json:"username"`                 //用户名
	CreateTime      []string `form:"create_time" json:"create_time"`           //创建时间
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminLogDeleteReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminLogDetailReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}
