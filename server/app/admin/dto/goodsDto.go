package dto

import (
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/conv"
)

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminGoodsPageReq struct {
	request.PageReq
	GoodsId    *uint    `form:"goods_id" json:"goods_id"`       //主键id
	GoodsName  string   `form:"goods_name" json:"goods_name"`   //商品名称
	SupplierId int      `form:"supplier_id" json:"supplier_id"` //供应商
	ClassId    int      `form:"class_id" json:"class_id"`       //所属分类
	Status     *int8    `form:"status" json:"status"`           //状态
	CreateTime []string `form:"create_time" json:"create_time"` //发布时间
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminGoodsAddReq struct {
	GoodsName  string     `form:"goods_name" json:"goods_name" `   //商品名称
	SupplierId int        `form:"supplier_id" json:"supplier_id" ` //供应商
	ClassId    int        `form:"class_id" json:"class_id" `       //所属分类
	Pic        string     `form:"pic" json:"pic" `                 //封面图
	SalePrice  float64    `form:"sale_price" json:"sale_price" `   //销售价
	Images     conv.Files `form:"images" json:"images" `           //图集
	Status     int8       `form:"status" json:"status" `           //状态
	Cd         string     `form:"cd" json:"cd" `                   //产地
	Store      int        `form:"store" json:"store" `             //库存
	Sortid     int        `form:"sortid" json:"sortid" `           //排序
	Detail     string     `form:"detail" json:"detail" `           //内容详情
	CreateTime string     `form:"create_time" json:"create_time" ` //发布时间
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminGoodsUpdateReq struct {
	GoodsId    uint       `form:"goods_id" json:"goods_id" validate:"required" label:"编号"` //主键id
	GoodsName  string     `form:"goods_name" json:"goods_name" `                           //商品名称
	SupplierId int        `form:"supplier_id" json:"supplier_id" `                         //供应商
	ClassId    int        `form:"class_id" json:"class_id" `                               //所属分类
	Pic        string     `form:"pic" json:"pic" `                                         //封面图
	SalePrice  float64    `form:"sale_price" json:"sale_price" `                           //销售价
	Images     conv.Files `form:"images" json:"images" `                                   //图集
	Status     int8       `form:"status" json:"status" `                                   //状态
	Cd         string     `form:"cd" json:"cd" `                                           //产地
	Store      int        `form:"store" json:"store" `                                     //库存
	Sortid     int        `form:"sortid" json:"sortid" `                                   //排序
	Detail     string     `form:"detail" json:"detail" `                                   //内容详情
	CreateTime string     `form:"create_time" json:"create_time" `                         //发布时间
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminGoodsGetUpdateInfoReq struct {
	GoodsId interface{} `form:"goods_id" json:"goods_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminGoodsDeleteReq struct {
	GoodsId interface{} `form:"goods_id" json:"goods_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminGoodsDetailReq struct {
	GoodsId interface{} `form:"goods_id" json:"goods_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(所属分类下拉联动)
 * @buildcode(true)
 */
type AdminGoodsClassIdReq struct {
	SupplierId int `form:"supplier_id" json:"supplier_id"` //供应商
}
