package dto

import (
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/conv"
)

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminMembePageReq struct {
	request.PageReq
	MembeId    *uint    `form:"membe_id" json:"membe_id"`       //主键id
	Username   string   `form:"username" json:"username"`       //用户名
	Sex        int      `form:"sex" json:"sex"`                 //性别
	Mobile     string   `form:"mobile" json:"mobile"`           //手机号
	Email      string   `form:"email" json:"email"`             //邮箱
	Status     *int8    `form:"status" json:"status"`           //状态
	Ssq        []string `form:"ssq" json:"ssq"`                 //省市区
	CreateTime []string `form:"create_time" json:"create_time"` //创建时间
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminMembeAddReq struct {
	Username   string   `form:"username" json:"username" validate:"required" label:"用户名"`    //用户名
	Sex        int      `form:"sex" json:"sex" validate:"required" label:"性别"`               //性别
	Mobile     string   `form:"mobile" json:"mobile" validate:"mobile,required" label:"手机号"` //手机号
	Pic        string   `form:"pic" json:"pic" `                                             //头像
	Email      string   `form:"email" json:"email" validate:"required,email" label:"邮箱"`     //邮箱
	Password   string   `form:"password" json:"password" `                                   //密码
	Amount     float64  `form:"amount" json:"amount" `                                       //积分
	Status     int8     `form:"status" json:"status" `                                       //状态
	Ssq        conv.Ssq `form:"ssq" json:"ssq" `                                             //省市区
	CreateTime string   `form:"create_time" json:"create_time" `                             //创建时间
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminMembeUpdateReq struct {
	MembeId    uint     `form:"membe_id" json:"membe_id" validate:"required" label:"编号"`     //主键id
	Username   string   `form:"username" json:"username" validate:"required" label:"用户名"`    //用户名
	Sex        int      `form:"sex" json:"sex" validate:"required" label:"性别"`               //性别
	Mobile     string   `form:"mobile" json:"mobile" validate:"mobile,required" label:"手机号"` //手机号
	Pic        string   `form:"pic" json:"pic" `                                             //头像
	Email      string   `form:"email" json:"email" validate:"required,email" label:"邮箱"`     //邮箱
	Amount     float64  `form:"amount" json:"amount" `                                       //积分
	Status     int8     `form:"status" json:"status" `                                       //状态
	Ssq        conv.Ssq `form:"ssq" json:"ssq" `                                             //省市区
	CreateTime string   `form:"create_time" json:"create_time" `                             //创建时间
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminMembeGetUpdateInfoReq struct {
	MembeId interface{} `form:"membe_id" json:"membe_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminMembeDeleteReq struct {
	MembeId interface{} `form:"membe_id" json:"membe_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminMembeDetailReq struct {
	MembeId interface{} `form:"membe_id" json:"membe_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(重置密码)
 * @buildcode(true)
 */
type AdminMembeResetPwdReq struct {
	MembeId  interface{} `form:"membe_id" json:"membe_id" validate:"required" label:"编号"` //主键id
	Password string      `form:"password" json:"password"`                                //密码
}

/**
 * @description(导入)
 * @buildcode(true)
 */
type AdminMembeImportReq struct {
	Username   string `form:"用户名" json:"用户名"`   //用户名
	Sex        string `form:"性别" json:"性别"`     //性别
	Mobile     string `form:"手机号" json:"手机号"`   //手机号
	Pic        string `form:"头像" json:"头像"`     //头像
	Email      string `form:"邮箱" json:"邮箱"`     //邮箱
	Password   string `form:"密码" json:"密码"`     //密码
	Amount     string `form:"积分" json:"积分"`     //积分
	Status     string `form:"状态" json:"状态"`     //状态
	Ssq        string `form:"省市区" json:"省市区"`   //省市区
	CreateTime string `form:"创建时间" json:"创建时间"` //创建时间
}

/**
 * @description(充值)
 * @buildcode(true)
 */
type AdminMembeJiaReq struct {
	MembeId interface{} `form:"membe_id" json:"membe_id" validate:"required" label:"编号"` //主键id
	Amount  float64     `form:"amount" json:"amount"`                                    //积分
}

/**
 * @description(扣除)
 * @buildcode(true)
 */
type AdminMembeJianReq struct {
	MembeId interface{} `form:"membe_id" json:"membe_id" validate:"required" label:"编号"` //主键id
	Amount  float64     `form:"amount" json:"amount"`                                    //积分
}

/**
 * @description(状态禁用)
 * @buildcode(true)
 */
type AdminMembeForbiddenReq struct {
	MembeId interface{} `form:"membe_id" json:"membe_id" validate:"required" label:"编号"` //主键id
}
