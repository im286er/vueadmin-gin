package dto

import "github.com/vueadmin/utils/common/request"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminGoodsCataPageReq struct {
	request.PageReq
	ClassId    *uint `form:"class_id" json:"class_id"`       //主键id
	SupplierId int   `form:"supplier_id" json:"supplier_id"` //供应商
	Status     *int8 `form:"status" json:"status"`           //状态
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminGoodsCataAddReq struct {
	ClassName  string `form:"class_name" json:"class_name" `   //分类名称
	SupplierId int    `form:"supplier_id" json:"supplier_id" ` //供应商
	Pid        int    `form:"pid" json:"pid" `                 //所属父类
	Status     int8   `form:"status" json:"status" `           //状态
	Sortid     int    `form:"sortid" json:"sortid" `           //排序
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminGoodsCataUpdateReq struct {
	ClassId    uint   `form:"class_id" json:"class_id" validate:"required" label:"编号"` //主键id
	ClassName  string `form:"class_name" json:"class_name" `                           //分类名称
	SupplierId int    `form:"supplier_id" json:"supplier_id" `                         //供应商
	Pid        int    `form:"pid" json:"pid" `                                         //所属父类
	Status     int8   `form:"status" json:"status" `                                   //状态
	Sortid     int    `form:"sortid" json:"sortid" `                                   //排序
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminGoodsCataGetUpdateInfoReq struct {
	ClassId interface{} `form:"class_id" json:"class_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminGoodsCataDeleteReq struct {
	ClassId interface{} `form:"class_id" json:"class_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminGoodsCataDetailReq struct {
	ClassId interface{} `form:"class_id" json:"class_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(所属父类下拉联动)
 * @buildcode(true)
 */
type AdminGoodsCataPidReq struct {
	SupplierId int `form:"supplier_id" json:"supplier_id"` //供应商
}
