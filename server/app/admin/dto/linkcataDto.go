package dto

import "github.com/vueadmin/utils/common/request"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminLinkcataPageReq struct {
	request.PageReq
	LinkcataId *uint  `form:"linkcata_id" json:"linkcata_id"` //主键id
	ClassName  string `form:"class_name" json:"class_name"`   //分类名称
	Status     *int8  `form:"status" json:"status"`           //状态
	Count      string `form:"count" json:"count"`             //分类下数量
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminLinkcataAddReq struct {
	ClassName string `form:"class_name" json:"class_name" ` //分类名称
	Status    int8   `form:"status" json:"status" `         //状态
	Jdt       int    `form:"jdt" json:"jdt" `               //进度条
	Count     string `form:"count" json:"count" `           //分类下数量
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminLinkcataUpdateReq struct {
	LinkcataId uint   `form:"linkcata_id" json:"linkcata_id" validate:"required" label:"编号"` //主键id
	ClassName  string `form:"class_name" json:"class_name" `                                 //分类名称
	Status     int8   `form:"status" json:"status" `                                         //状态
	Jdt        int    `form:"jdt" json:"jdt" `                                               //进度条
	Count      string `form:"count" json:"count" `                                           //分类下数量
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminLinkcataGetUpdateInfoReq struct {
	LinkcataId interface{} `form:"linkcata_id" json:"linkcata_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminLinkcataDeleteReq struct {
	LinkcataId interface{} `form:"linkcata_id" json:"linkcata_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminLinkcataDetailReq struct {
	LinkcataId interface{} `form:"linkcata_id" json:"linkcata_id" validate:"required" label:"编号"` //主键id
}
