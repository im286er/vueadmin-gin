package dto

import "github.com/vueadmin/utils/common/request"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminTestPageReq struct {
	request.PageReq
	TestId      *uint  `form:"test_id" json:"test_id"`         //主键id
	Title       string `form:"title" json:"title"`             //标题
	OnlyTrashed bool   `form:"onlyTrashed" json:"onlyTrashed"` //软删除查询标记
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminTestAddReq struct {
	Title  string `form:"title" json:"title" `   //标题
	Status int8   `form:"status" json:"status" ` //状态
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminTestUpdateReq struct {
	TestId uint   `form:"test_id" json:"test_id" validate:"required" label:"编号"` //主键id
	Title  string `form:"title" json:"title" `                                   //标题
	Status int8   `form:"status" json:"status" `                                 //状态
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminTestGetUpdateInfoReq struct {
	TestId interface{} `form:"test_id" json:"test_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminTestDeleteReq struct {
	TestId      interface{} `form:"test_id" json:"test_id" validate:"required" label:"编号"` //主键id
	OnlyTrashed bool        `form:"onlyTrashed" json:"onlyTrashed"`                        //物理删除标记
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminTestDetailReq struct {
	TestId interface{} `form:"test_id" json:"test_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(软删除恢复数据)
 * @buildcode(true)
 */
type AdminTestRestoreReq struct {
	TestId interface{} `form:"test_id" json:"test_id" validate:"required" label:"编号"` //主键id
}
