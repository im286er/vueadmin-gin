package dto

import "github.com/vueadmin/utils/common/request"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminCarPageReq struct {
	request.PageReq
	CarId   *uint `form:"car_id" json:"car_id"`     //主键id
	MembeId int   `form:"membe_id" json:"membe_id"` //用户id
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminCarAddReq struct {
	MembeId    int    `form:"membe_id" json:"membe_id" `       //用户id
	CarNo      string `form:"car_no" json:"car_no" `           //车牌号
	CreateTime string `form:"create_time" json:"create_time" ` //创建时间
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminCarUpdateReq struct {
	CarId      uint   `form:"car_id" json:"car_id" validate:"required" label:"编号"` //主键id
	MembeId    int    `form:"membe_id" json:"membe_id" `                           //用户id
	CarNo      string `form:"car_no" json:"car_no" `                               //车牌号
	CreateTime string `form:"create_time" json:"create_time" `                     //创建时间
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminCarGetUpdateInfoReq struct {
	CarId interface{} `form:"car_id" json:"car_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminCarDeleteReq struct {
	CarId interface{} `form:"car_id" json:"car_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminCarDetailReq struct {
	CarId interface{} `form:"car_id" json:"car_id" validate:"required" label:"编号"` //主键id
}
