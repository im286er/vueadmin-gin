package dto

import "github.com/vueadmin/utils/common/request"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminNodePageReq struct {
	request.PageReq
	NodeId *uint `form:"node_id" json:"node_id"` //主键id
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminNodeAddReq struct {
	Pid           int    `form:"pid" json:"pid" `                       //所属父类
	Title         string `form:"title" json:"title" `                   //节点名称
	Type          int    `form:"type" json:"type" `                     //类型
	ComponentPath string `form:"component_path" json:"component_path" ` //组件路径
	Status        int8   `form:"status" json:"status" `                 //状态
	Icon          string `form:"icon" json:"icon" `                     //图标
	Sortid        int    `form:"sortid" json:"sortid" `                 //排序
	Path          string `form:"path" json:"path" `                     //节点标识
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminNodeUpdateReq struct {
	NodeId        uint   `form:"node_id" json:"node_id" validate:"required" label:"编号"` //主键id
	Pid           int    `form:"pid" json:"pid" `                                       //所属父类
	Title         string `form:"title" json:"title" `                                   //节点名称
	Type          int    `form:"type" json:"type" `                                     //类型
	ComponentPath string `form:"component_path" json:"component_path" `                 //组件路径
	Status        int8   `form:"status" json:"status" `                                 //状态
	Icon          string `form:"icon" json:"icon" `                                     //图标
	Sortid        int    `form:"sortid" json:"sortid" `                                 //排序
	Path          string `form:"path" json:"path" `                                     //节点标识
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminNodeGetUpdateInfoReq struct {
	NodeId interface{} `form:"node_id" json:"node_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminNodeDeleteReq struct {
	NodeId interface{} `form:"node_id" json:"node_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminNodeDetailReq struct {
	NodeId interface{} `form:"node_id" json:"node_id" validate:"required" label:"编号"` //主键id
}
