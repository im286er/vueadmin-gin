package dto

import "github.com/vueadmin/utils/common/request"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminAdminuserPageReq struct {
	request.PageReq
	UserId *uint  `form:"user_id" json:"user_id"` //主键id
	User   string `form:"user" json:"user"`       //用户名
	RoleId int    `form:"role_id" json:"role_id"` //所属角色
	Status *int8  `form:"status" json:"status"`   //状态
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminAdminuserAddReq struct {
	Name       string `form:"name" json:"name" `                                       //用户姓名
	User       string `form:"user" json:"user" validate:"required" label:"用户名"`        //用户名
	Pwd        string `form:"pwd" json:"pwd" validate:"required" label:"密码"`           //密码
	RoleId     int    `form:"role_id" json:"role_id" validate:"required" label:"所属角色"` //所属角色
	Note       string `form:"note" json:"note" `                                       //备注
	Status     int8   `form:"status" json:"status" `                                   //状态
	CreateTime string `form:"create_time" json:"create_time" `                         //创建时间
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminAdminuserUpdateReq struct {
	UserId     uint   `form:"user_id" json:"user_id" validate:"required" label:"编号"`   //主键id
	Name       string `form:"name" json:"name" `                                       //用户姓名
	User       string `form:"user" json:"user" validate:"required" label:"用户名"`        //用户名
	RoleId     int    `form:"role_id" json:"role_id" validate:"required" label:"所属角色"` //所属角色
	Note       string `form:"note" json:"note" `                                       //备注
	Status     int8   `form:"status" json:"status" `                                   //状态
	CreateTime string `form:"create_time" json:"create_time" `                         //创建时间
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminAdminuserGetUpdateInfoReq struct {
	UserId interface{} `form:"user_id" json:"user_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminAdminuserDeleteReq struct {
	UserId interface{} `form:"user_id" json:"user_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminAdminuserDetailReq struct {
	UserId interface{} `form:"user_id" json:"user_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(重置密码)
 * @buildcode(true)
 */
type AdminAdminuserResetPwdReq struct {
	UserId interface{} `form:"user_id" json:"user_id" validate:"required" label:"编号"` //主键id
	Pwd    string      `form:"pwd" json:"pwd"`                                        //密码
}
