package dto

import (
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/conv"
)

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminRolePageReq struct {
	request.PageReq
	RoleId *uint  `form:"role_id" json:"role_id"` //主键id
	Name   string `form:"name" json:"name"`       //角色名称
	Status *int8  `form:"status" json:"status"`   //状态
}

/**
 * @description(添加)
 * @buildcode(false)
 */
type AdminRoleAddReq struct {
	Name        string       `form:"name" json:"name" validate:"required" label:"角色名称"` //角色名称
	Status      int8         `form:"status" json:"status" `                             //状态
	Description string       `form:"description" json:"description" `                   //描述
	Access      conv.Strings `form:"access" json:"access"`                              //权限节点
}

/**
 * @description(修改)
 * @buildcode(false)
 */
type AdminRoleUpdateReq struct {
	RoleId      uint         `form:"role_id" json:"role_id" validate:"required" label:"编号"` //主键id
	Name        string       `form:"name" json:"name" validate:"required" label:"角色名称"`     //角色名称
	Status      int8         `form:"status" json:"status" `                                 //状态
	Description string       `form:"description" json:"description" `                       //描述
	Access      conv.Strings `form:"access" json:"access"`                                  //权限节点
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminRoleGetUpdateInfoReq struct {
	RoleId interface{} `form:"role_id" json:"role_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminRoleDeleteReq struct {
	RoleId interface{} `form:"role_id" json:"role_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminRoleDetailReq struct {
	RoleId interface{} `form:"role_id" json:"role_id" validate:"required" label:"编号"` //主键id
}
