package dto

import "github.com/vueadmin/utils/common/request"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminCrontabPageReq struct {
	request.PageReq
	CrontabId *uint  `form:"crontab_id" json:"crontab_id"` //主键id
	Name      string `form:"name" json:"name"`             //任务名称
	Type      int    `form:"type" json:"type"`             //任务类型
	Status    *int8  `form:"status" json:"status"`         //状态
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminCrontabAddReq struct {
	Name   string `form:"name" json:"name" validate:"required" label:"任务名称"`     //任务名称
	Type   int    `form:"type" json:"type" validate:"required" label:"任务类型"`     //任务类型
	Rule   string `form:"rule" json:"rule" validate:"required" label:"规则"`       //规则
	Status int8   `form:"status" json:"status" `                                 //状态
	Target string `form:"target" json:"target" validate:"required" label:"调用目标"` //调用目标
	Remark string `form:"remark" json:"remark" `                                 //描述
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminCrontabUpdateReq struct {
	CrontabId uint   `form:"crontab_id" json:"crontab_id" validate:"required" label:"编号"` //主键id
	Name      string `form:"name" json:"name" validate:"required" label:"任务名称"`           //任务名称
	Type      int    `form:"type" json:"type" validate:"required" label:"任务类型"`           //任务类型
	Rule      string `form:"rule" json:"rule" validate:"required" label:"规则"`             //规则
	Status    int8   `form:"status" json:"status" `                                       //状态
	Target    string `form:"target" json:"target" validate:"required" label:"调用目标"`       //调用目标
	Remark    string `form:"remark" json:"remark" `                                       //描述
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminCrontabGetUpdateInfoReq struct {
	CrontabId interface{} `form:"crontab_id" json:"crontab_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminCrontabDeleteReq struct {
	CrontabId interface{} `form:"crontab_id" json:"crontab_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminCrontabDetailReq struct {
	CrontabId interface{} `form:"crontab_id" json:"crontab_id" validate:"required" label:"编号"` //主键id
}
