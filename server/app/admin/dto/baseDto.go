package dto

// 重置密码
type AdminResetPwdReq struct {
	Password   string `json:"password"  validate:"required" label:"密码"`
	Repassword string `json:"repassword"  validate:"required,same" target:"Password" label:"确认密码"`
}

// 获取省市区参数
type Shenshiqu struct {
	Type int `form:"type" json:"type"` //层级 1:三级省市区 2:二级省市  3:省
}
