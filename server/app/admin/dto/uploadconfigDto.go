package dto

import "github.com/vueadmin/utils/common/request"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminUploadconfigPageReq struct {
	request.PageReq
	Id          *uint  `form:"id" json:"id"`                     //主键id
	Title       string `form:"title" json:"title"`               //配置名称
	ThumbStatus *int8  `form:"thumb_status" json:"thumb_status"` //生成缩略图
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminUploadconfigAddReq struct {
	Title       string `form:"title" json:"title" validate:"required" label:"配置名称"` //配置名称
	ThumbStatus int8   `form:"thumb_status" json:"thumb_status" `                   //生成缩略图
	ThumbWidth  string `form:"thumb_width" json:"thumb_width" `                     //缩略图宽
	ThumbHeight string `form:"thumb_height" json:"thumb_height" `                   //缩略图高
	ThumbType   int    `form:"thumb_type" json:"thumb_type" `                       //缩放类型
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminUploadconfigUpdateReq struct {
	Id          uint   `form:"id" json:"id" validate:"required" label:"编号"`         //主键id
	Title       string `form:"title" json:"title" validate:"required" label:"配置名称"` //配置名称
	ThumbStatus int8   `form:"thumb_status" json:"thumb_status" `                   //生成缩略图
	ThumbWidth  string `form:"thumb_width" json:"thumb_width" `                     //缩略图宽
	ThumbHeight string `form:"thumb_height" json:"thumb_height" `                   //缩略图高
	ThumbType   int    `form:"thumb_type" json:"thumb_type" `                       //缩放类型
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminUploadconfigGetUpdateInfoReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminUploadconfigDeleteReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminUploadconfigDetailReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}
