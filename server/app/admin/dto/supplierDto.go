package dto

import "github.com/vueadmin/utils/common/request"

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminSupplierPageReq struct {
	request.PageReq
	SupplierId   *uint    `form:"supplier_id" json:"supplier_id"`     //主键id
	SupplierName string   `form:"supplier_name" json:"supplier_name"` //供应商名称
	Status       *int8    `form:"status" json:"status"`               //状态
	Username     string   `form:"username" json:"username"`           //用户名
	CreateTime   []string `form:"create_time" json:"create_time"`     //创建时间
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminSupplierAddReq struct {
	SupplierName string `form:"supplier_name" json:"supplier_name" ` //供应商名称
	Status       int8   `form:"status" json:"status" `               //状态
	Username     string `form:"username" json:"username" `           //用户名
	Password     string `form:"password" json:"password" `           //密码
	CreateTime   string `form:"create_time" json:"create_time" `     //创建时间
}

/**
 * @description(修改)
 * @buildcode(true)
 */
type AdminSupplierUpdateReq struct {
	SupplierId   uint   `form:"supplier_id" json:"supplier_id" validate:"required" label:"编号"` //主键id
	SupplierName string `form:"supplier_name" json:"supplier_name" `                           //供应商名称
	Status       int8   `form:"status" json:"status" `                                         //状态
	Username     string `form:"username" json:"username" `                                     //用户名
	CreateTime   string `form:"create_time" json:"create_time" `                               //创建时间
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminSupplierGetUpdateInfoReq struct {
	SupplierId interface{} `form:"supplier_id" json:"supplier_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminSupplierDeleteReq struct {
	SupplierId interface{} `form:"supplier_id" json:"supplier_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
type AdminSupplierDetailReq struct {
	SupplierId interface{} `form:"supplier_id" json:"supplier_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(重置密码)
 * @buildcode(true)
 */
type AdminSupplierResetPwdReq struct {
	SupplierId interface{} `form:"supplier_id" json:"supplier_id" validate:"required" label:"编号"` //主键id
	Password   string      `form:"password" json:"password"`                                      //密码
}
