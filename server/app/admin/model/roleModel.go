package model

import (
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

/**
 * @description(角色管理模型)
 * @buildcode(false)
 */
type AdminRole struct {
	RoleId      uint         `gorm:"primaryKey" json:"role_id"` //编号
	Name        string       `json:"name"`                      //角色名称
	Status      int8         `json:"status"`                    //状态
	Description string       `json:"description"`               //描述
	Access      conv.Strings `json:"access"`                    //权限节点
}

func (AdminRole) TableName() string {
	return global.CONF.Db.Prefix + "role"
}
