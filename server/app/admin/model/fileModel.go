package model

import (
	"github.com/vueadmin/global"
)

type AdminFile struct {
	Id         uint   `gorm:"primaryKey" json:"id"` //id
	Filepath   string `json:"filepath"`             //图片路径
	Hash       string `json:"hash"`                 //文件hash值
	CreateTime int64  `json:"create_time"`          //创建时间
	Disk       string `json:"disk"`                 //存储方式
	Type       int8   `json:"type"`                 //文件类型
}

func (AdminFile) TableName() string {
	return global.CONF.Db.Prefix + "file"
}
