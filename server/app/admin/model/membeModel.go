package model

import (
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

/**
 * @description(会员管理模型)
 * @buildcode(true)
 */
type AdminMembe struct {
	MembeId    uint     `gorm:"primaryKey" json:"membe_id"` //编号
	Username   string   `json:"username"`                   //用户名
	Sex        int      `json:"sex"`                        //性别
	Mobile     string   `json:"mobile"`                     //手机号
	Pic        string   `json:"pic"`                        //头像
	Email      string   `json:"email"`                      //邮箱
	Password   string   `json:"password"`                   //密码
	Amount     float64  `json:"amount"`                     //积分
	Status     int8     `json:"status"`                     //状态
	Ssq        conv.Ssq `json:"ssq"`                        //省市区
	CreateTime int64    `json:"create_time"`                //创建时间
}

func (AdminMembe) TableName() string {
	return global.CONF.Db.Prefix + "membe"
}
