package model

import (
	"github.com/vueadmin/global"
)

type Node struct {
	NodeId        uint    `gorm:"primaryKey" form:"node_id" json:"node_id"`
	Pid           int64   `form:"pid" json:"pid"`
	Title         string  `form:"title" json:"title"`
	Status        int8    `form:"status" json:"status"`
	Sortid        int64   `form:"sortid" json:"sortid"`
	ComponentPath string  `json:"component_path"`
	Path          string  `json:"path"`
	Access        string  `form:"access" json:"access" gorm:"-"`
	Icon          string  `form:"icon" json:"icon"`
	Children      []*Node `json:"children" gorm:"-"`
}

func (Node) TableName() string {
	return global.CONF.Db.Prefix + "node"
}
