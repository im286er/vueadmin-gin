package model

import "github.com/vueadmin/global"

/**
 * @description(定时任务模型)
 * @buildcode(true)
 */
type AdminCrontab struct {
	CrontabId   uint   `gorm:"primaryKey" json:"crontab_id"` //编号
	Name        string `json:"name"`                         //任务名称
	Type        int    `json:"type"`                         //任务类型
	Rule        string `json:"rule"`                         //规则
	Status      int8   `json:"status"`                       //状态
	Target      string `json:"target"`                       //调用目标
	Remark      string `json:"remark"`                       //描述
	LastRunTime int64  `json:"last_run_time"`                //最后执行时间
}

func (AdminCrontab) TableName() string {
	return global.CONF.Db.Prefix + "crontab"
}
