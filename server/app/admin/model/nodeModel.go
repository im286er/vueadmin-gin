package model

import "github.com/vueadmin/global"

/**
 * @description(节点管理模型)
 * @buildcode(true)
 */
type AdminNode struct {
	NodeId        uint   `gorm:"primaryKey" json:"node_id"` //编号
	Pid           int    `json:"pid"`                       //所属父类
	Title         string `json:"title"`                     //节点名称
	Type          int    `json:"type"`                      //类型
	ComponentPath string `json:"component_path"`            //组件路径
	Status        int8   `json:"status"`                    //状态
	Icon          string `json:"icon"`                      //图标
	Sortid        int    `json:"sortid"`                    //排序
	Path          string `json:"path"`                      //节点标识
}

/**
 * @description(节点管理数据表)
 * @buildcode(true)
 */
func (AdminNode) TableName() string {
	return global.CONF.Db.Prefix + "node"
}
