package model

import "github.com/vueadmin/global"

/**
 * @description(用户管理模型)
 * @buildcode(true)
 */
type AdminAdminuser struct {
	UserId     uint   `gorm:"primaryKey" json:"user_id"` //编号
	Name       string `json:"name"`                      //用户姓名
	User       string `json:"user"`                      //用户名
	Pwd        string `json:"pwd"`                       //密码
	RoleId     int    `json:"role_id"`                   //所属角色
	Note       string `json:"note"`                      //备注
	Status     int8   `json:"status"`                    //状态
	CreateTime int64  `json:"create_time"`               //创建时间
}

func (AdminAdminuser) TableName() string {
	return global.CONF.Db.Prefix + "admin_user"
}
