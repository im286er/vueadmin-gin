package model

import "github.com/vueadmin/global"

/**
 * @description(友情链接分类模型)
 * @buildcode(true)
 */
type AdminLinkcata struct {
	LinkcataId uint   `gorm:"primaryKey" json:"linkcata_id"` //编号
	ClassName  string `json:"class_name"`                    //分类名称
	Status     int8   `json:"status"`                        //状态
	Jdt        int    `json:"jdt"`                           //进度条
	Count      string `json:"count"`                         //分类下数量
}

/**
 * @description(友情链接分类数据表)
 * @buildcode(true)
 */
func (AdminLinkcata) TableName() string {
	return global.CONF.Db.Prefix + "linkcata"
}
