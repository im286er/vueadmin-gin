package model

import "github.com/vueadmin/global"

/**
 * @description(基本配置模型)
 * @buildcode(true)
 */
type AdminBaseconfig struct {
	Name string `json:"name"`
	Data string `json:"data"`
}

func (AdminBaseconfig) TableName() string {
	return global.CONF.Db.Prefix + "base_config"
}
