package model

import "github.com/vueadmin/global"

/**
 * @description(缩略图配置模型)
 * @buildcode(true)
 */
type AdminUploadconfig struct {
	Id          uint   `gorm:"primaryKey" json:"id"` //编号
	Title       string `json:"title"`                //配置名称
	ThumbStatus int8   `json:"thumb_status"`         //生成缩略图
	ThumbWidth  string `json:"thumb_width"`          //缩略图宽
	ThumbHeight string `json:"thumb_height"`         //缩略图高
	ThumbType   int    `json:"thumb_type"`           //缩放类型
}

func (AdminUploadconfig) TableName() string {
	return global.CONF.Db.Prefix + "upload_config"
}
