package model

import "github.com/vueadmin/global"

/**
 * @description(供应商管理模型)
 * @buildcode(true)
 */
type AdminSupplier struct {
	SupplierId   uint   `gorm:"primaryKey" json:"supplier_id"` //编号
	SupplierName string `json:"supplier_name"`                 //供应商名称
	Status       int8   `json:"status"`                        //状态
	Username     string `json:"username"`                      //用户名
	Password     string `json:"password"`                      //密码
	CreateTime   int64  `json:"create_time"`                   //创建时间
}

/**
 * @description(供应商管理数据表)
 * @buildcode(true)
 */
func (AdminSupplier) TableName() string {
	return global.CONF.Db.Prefix + "supplier"
}
