package model

import "github.com/vueadmin/global"

/**
 * @description(日志管理模型)
 * @buildcode(true)
 */
type AdminLog struct {
	Id              uint   `gorm:"primaryKey" json:"id"` //编号
	ApplicationName string `json:"application_name"`     //所属应用
	Type            int    `json:"type"`                 //日志类型
	Username        string `json:"username"`             //用户名
	Url             string `json:"url"`                  //请求url
	Ip              string `json:"ip"`                   //客户端ip
	Useragent       string `json:"useragent"`            //浏览器信息
	Content         string `json:"content"`              //请求内容
	Errmsg          string `json:"errmsg"`               //异常信息
	CreateTime      int64  `json:"create_time"`          //创建时间
}

func (AdminLog) TableName() string {
	return global.CONF.Db.Prefix + "log"
}
