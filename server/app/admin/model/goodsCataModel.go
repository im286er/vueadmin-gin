package model

import "github.com/vueadmin/global"

/**
 * @description(商品分类模型)
 * @buildcode(true)
 */
type AdminGoodsCata struct {
	ClassId    uint   `gorm:"primaryKey" json:"class_id"` //编号
	ClassName  string `json:"class_name"`                 //分类名称
	SupplierId int    `json:"supplier_id"`                //供应商
	Pid        int    `json:"pid"`                        //所属父类
	Status     int8   `json:"status"`                     //状态
	Sortid     int    `json:"sortid"`                     //排序
}

/**
 * @description(商品分类数据表)
 * @buildcode(true)
 */
func (AdminGoodsCata) TableName() string {
	return global.CONF.Db.Prefix + "goods_cata"
}
