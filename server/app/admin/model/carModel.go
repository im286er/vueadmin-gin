package model

import "github.com/vueadmin/global"

/**
 * @description(车牌管理模型)
 * @buildcode(true)
 */
type AdminCar struct {
	CarId      uint   `gorm:"primaryKey" json:"car_id"` //编号
	MembeId    int    `json:"membe_id"`                 //用户id
	CarNo      string `json:"car_no"`                   //车牌号
	CreateTime int64  `json:"create_time"`              //创建时间
}

/**
 * @description(车牌管理数据表)
 * @buildcode(true)
 */
func (AdminCar) TableName() string {
	return global.CONF.Db.Prefix + "car"
}
