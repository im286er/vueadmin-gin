package model

import "github.com/vueadmin/global"

/**
 * @description(友情链接模型)
 * @buildcode(true)
 */
type AdminLink struct {
	LinkId     uint   `gorm:"primaryKey" json:"link_id"` //编号
	Title      string `json:"title"`                     //链接名称
	Url        string `json:"url"`                       //链接地址
	LinkcataId int    `json:"linkcata_id"`               //所属分类
	Logo       string `json:"logo"`                      //logo
	Status     int8   `json:"status"`                    //状态
	Sortid     int    `json:"sortid"`                    //排序
	CreateTime int64  `json:"create_time"`               //创建时间
}

/**
 * @description(友情链接数据表)
 * @buildcode(true)
 */
func (AdminLink) TableName() string {
	return global.CONF.Db.Prefix + "link"
}
