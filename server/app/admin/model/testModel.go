package model

import (
	"github.com/vueadmin/global"
	"gorm.io/gorm"
)

/**
 * @description(软删除回收站模型)
 * @buildcode(true)
 */
type AdminTest struct {
	TestId    uint           `gorm:"primaryKey" json:"test_id"` //编号
	Title     string         `json:"title"`                     //标题
	Status    int8           `json:"status"`                    //状态
	DeletedAt gorm.DeletedAt `json:"deleted_at"`                //软删除标记
}

/**
 * @description(软删除回收站数据表)
 * @buildcode(true)
 */
func (AdminTest) TableName() string {
	return global.CONF.Db.Prefix + "test"
}
