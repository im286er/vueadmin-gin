package model

import (
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

/**
 * @description(商品管理模型)
 * @buildcode(true)
 */
type AdminGoods struct {
	GoodsId    uint       `gorm:"primaryKey" json:"goods_id"` //编号
	GoodsName  string     `json:"goods_name"`                 //商品名称
	SupplierId int        `json:"supplier_id"`                //供应商
	ClassId    int        `json:"class_id"`                   //所属分类
	Pic        string     `json:"pic"`                        //封面图
	SalePrice  float64    `json:"sale_price"`                 //销售价
	Images     conv.Files `json:"images"`                     //图集
	Status     int8       `json:"status"`                     //状态
	Cd         string     `json:"cd"`                         //产地
	Store      int        `json:"store"`                      //库存
	Sortid     int        `json:"sortid"`                     //排序
	Detail     string     `json:"detail"`                     //内容详情
	CreateTime int64      `json:"create_time"`                //发布时间
}

/**
 * @description(商品管理数据表)
 * @buildcode(true)
 */
func (AdminGoods) TableName() string {
	return global.CONF.Db.Prefix + "goods"
}
