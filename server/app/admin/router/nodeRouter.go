package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminNode struct{}

/**
 * @description(节点管理路由)
 * @buildcode(true)
 */
func (AdminNode) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Node/index", controller.AdminNode.GetPageList)           //数据列表
		r.POST("/admin/Node/updateExt", controller.AdminNode.UpdateExt)         //修改排序开关
		r.POST("/admin/Node/add", controller.AdminNode.Add)                     //添加
		r.POST("/admin/Node/update", controller.AdminNode.Update)               //修改
		r.POST("/admin/Node/getUpdateInfo", controller.AdminNode.GetUpdateInfo) //修改
		r.POST("/admin/Node/delete", controller.AdminNode.Delete)               //删除
		r.POST("/admin/Node/detail", controller.AdminNode.Detail)               //查看详情
		r.POST("/admin/Node/getFieldList", controller.AdminNode.GetFieldList)   //查询列表
	}
}
