package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminBaseconfig struct{}

/**
 * @description(基本配置路由)
 * @buildcode(true)
 */
func (AdminBaseconfig) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Baseconfig/index", controller.AdminBaseconfig.Index)   //配置表单
		r.POST("/admin/Baseconfig/update", controller.AdminBaseconfig.Update) //配置表单
	}
}
