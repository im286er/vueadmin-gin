package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminTest struct{}

/**
 * @description(软删除回收站路由)
 * @buildcode(true)
 */
func (AdminTest) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Test/index", controller.AdminTest.GetPageList)           //数据列表
		r.POST("/admin/Test/updateExt", controller.AdminTest.UpdateExt)         //修改排序开关
		r.POST("/admin/Test/add", controller.AdminTest.Add)                     //添加
		r.POST("/admin/Test/update", controller.AdminTest.Update)               //修改
		r.POST("/admin/Test/getUpdateInfo", controller.AdminTest.GetUpdateInfo) //修改
		r.POST("/admin/Test/delete", controller.AdminTest.Delete)               //删除
		r.POST("/admin/Test/detail", controller.AdminTest.Detail)               //查看详情
		r.POST("/admin/Test/restore", controller.AdminTest.Restore)             //回收站恢复数据
	}
}
