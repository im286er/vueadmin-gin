package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminGoods struct{}

/**
 * @description(商品管理路由)
 * @buildcode(true)
 */
func (AdminGoods) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Goods/index", controller.AdminGoods.GetPageList)           //数据列表
		r.POST("/admin/Goods/updateExt", controller.AdminGoods.UpdateExt)         //修改排序开关
		r.POST("/admin/Goods/add", controller.AdminGoods.Add)                     //添加
		r.POST("/admin/Goods/update", controller.AdminGoods.Update)               //修改
		r.POST("/admin/Goods/getUpdateInfo", controller.AdminGoods.GetUpdateInfo) //修改
		r.POST("/admin/Goods/delete", controller.AdminGoods.Delete)               //删除
		r.POST("/admin/Goods/detail", controller.AdminGoods.Detail)               //查看详情
		r.POST("/admin/Goods/getFieldList", controller.AdminGoods.GetFieldList)   //查询列表
		r.POST("/admin/Goods/getClassId", controller.AdminGoods.GetClassIdList)   //查询所属分类联动
	}
}
