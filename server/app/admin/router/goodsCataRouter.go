package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminGoodsCata struct{}

/**
 * @description(商品分类路由)
 * @buildcode(true)
 */
func (AdminGoodsCata) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/GoodsCata/index", controller.AdminGoodsCata.GetPageList)           //数据列表
		r.POST("/admin/GoodsCata/updateExt", controller.AdminGoodsCata.UpdateExt)         //修改排序开关
		r.POST("/admin/GoodsCata/add", controller.AdminGoodsCata.Add)                     //添加
		r.POST("/admin/GoodsCata/update", controller.AdminGoodsCata.Update)               //修改
		r.POST("/admin/GoodsCata/getUpdateInfo", controller.AdminGoodsCata.GetUpdateInfo) //修改
		r.POST("/admin/GoodsCata/delete", controller.AdminGoodsCata.Delete)               //删除
		r.POST("/admin/GoodsCata/detail", controller.AdminGoodsCata.Detail)               //查看详情
		r.POST("/admin/GoodsCata/getFieldList", controller.AdminGoodsCata.GetFieldList)   //查询列表
		r.POST("/admin/GoodsCata/getPid", controller.AdminGoodsCata.GetPidList)           //查询所属父类联动
	}
}
