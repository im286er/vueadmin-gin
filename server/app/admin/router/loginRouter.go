package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
	"github.com/vueadmin/pkg/upload"
)

type AdminLogin struct{}

func (AdminLogin) Router(r *gin.RouterGroup) {
	r.POST("/admin/Login/verify", middleware.BeforAction(), controller.AdminLogin.Captcha) //验证码信息
	r.POST("/admin/Login/login", controller.AdminLogin.Login)                              //登录接口
	r.POST("/admin/Index/logout", controller.AdminIndex.Logout)                            //退出
	r.POST("/admin/Base/aliOssCallBack", upload.Upload.AliOssCallBack)                     //阿里云客户端上传回调获取返回地址
}
