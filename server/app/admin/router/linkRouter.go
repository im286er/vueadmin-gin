package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminLink struct{}

/**
 * @description(友情链接路由)
 * @buildcode(true)
 */
func (AdminLink) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Link/index", controller.AdminLink.GetPageList)           //数据列表
		r.POST("/admin/Link/updateExt", controller.AdminLink.UpdateExt)         //修改排序开关
		r.POST("/admin/Link/add", controller.AdminLink.Add)                     //添加
		r.POST("/admin/Link/update", controller.AdminLink.Update)               //修改
		r.POST("/admin/Link/getUpdateInfo", controller.AdminLink.GetUpdateInfo) //修改
		r.POST("/admin/Link/delete", controller.AdminLink.Delete)               //删除
		r.POST("/admin/Link/detail", controller.AdminLink.Detail)               //查看详情
		r.POST("/admin/Link/getFieldList", controller.AdminLink.GetFieldList)   //查询列表
	}
}
