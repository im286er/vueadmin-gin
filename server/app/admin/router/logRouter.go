package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminLog struct{}

/**
 * @description(日志管理路由)
 * @buildcode(true)
 */
func (AdminLog) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Log/index", controller.AdminLog.GetPageList)         //数据列表
		r.POST("/admin/Log/delete", controller.AdminLog.Delete)             //删除
		r.POST("/admin/Log/detail", controller.AdminLog.Detail)             //查看详情
		r.POST("/admin/Log/export", controller.AdminLog.Export)             //导出
		r.POST("/admin/Log/getFieldList", controller.AdminLog.GetFieldList) //查询列表
	}
}
