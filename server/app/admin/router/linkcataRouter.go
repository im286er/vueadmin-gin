package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminLinkcata struct{}

/**
 * @description(友情链接分类路由)
 * @buildcode(true)
 */
func (AdminLinkcata) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Linkcata/index", controller.AdminLinkcata.GetPageList)           //数据列表
		r.POST("/admin/Linkcata/updateExt", controller.AdminLinkcata.UpdateExt)         //修改排序开关
		r.POST("/admin/Linkcata/add", controller.AdminLinkcata.Add)                     //添加
		r.POST("/admin/Linkcata/update", controller.AdminLinkcata.Update)               //修改
		r.POST("/admin/Linkcata/getUpdateInfo", controller.AdminLinkcata.GetUpdateInfo) //修改
		r.POST("/admin/Linkcata/delete", controller.AdminLinkcata.Delete)               //删除
		r.POST("/admin/Linkcata/detail", controller.AdminLinkcata.Detail)               //查看详情
	}
}
