package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminCrontab struct{}

/**
 * @description(定时任务路由)
 * @buildcode(true)
 */
func (AdminCrontab) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Crontab/index", controller.AdminCrontab.GetPageList)           //数据列表
		r.POST("/admin/Crontab/updateExt", controller.AdminCrontab.UpdateExt)         //修改排序开关
		r.POST("/admin/Crontab/add", controller.AdminCrontab.Add)                     //添加
		r.POST("/admin/Crontab/update", controller.AdminCrontab.Update)               //修改
		r.POST("/admin/Crontab/getUpdateInfo", controller.AdminCrontab.GetUpdateInfo) //修改
		r.POST("/admin/Crontab/delete", controller.AdminCrontab.Delete)               //删除
		r.POST("/admin/Crontab/detail", controller.AdminCrontab.Detail)               //查看详情
	}
}
