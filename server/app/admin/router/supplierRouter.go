package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminSupplier struct{}

/**
 * @description(供应商管理路由)
 * @buildcode(true)
 */
func (AdminSupplier) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Supplier/index", controller.AdminSupplier.GetPageList)           //数据列表
		r.POST("/admin/Supplier/updateExt", controller.AdminSupplier.UpdateExt)         //修改排序开关
		r.POST("/admin/Supplier/add", controller.AdminSupplier.Add)                     //添加
		r.POST("/admin/Supplier/update", controller.AdminSupplier.Update)               //修改
		r.POST("/admin/Supplier/getUpdateInfo", controller.AdminSupplier.GetUpdateInfo) //修改
		r.POST("/admin/Supplier/delete", controller.AdminSupplier.Delete)               //删除
		r.POST("/admin/Supplier/detail", controller.AdminSupplier.Detail)               //查看详情
		r.POST("/admin/Supplier/resetPwd", controller.AdminSupplier.ResetPwd)           //重置密码
	}
}
