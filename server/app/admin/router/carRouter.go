package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminCar struct{}

/**
 * @description(车牌管理路由)
 * @buildcode(true)
 */
func (AdminCar) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Car/index", controller.AdminCar.GetPageList)           //数据列表
		r.POST("/admin/Car/updateExt", controller.AdminCar.UpdateExt)         //修改排序开关
		r.POST("/admin/Car/add", controller.AdminCar.Add)                     //添加
		r.POST("/admin/Car/update", controller.AdminCar.Update)               //修改
		r.POST("/admin/Car/getUpdateInfo", controller.AdminCar.GetUpdateInfo) //修改
		r.POST("/admin/Car/delete", controller.AdminCar.Delete)               //删除
		r.POST("/admin/Car/detail", controller.AdminCar.Detail)               //查看详情
	}
}
