package router

var AdminRouter = []interface{}{
	&AdminBase{},
	&AdminLogin{},
	&AdminUploadconfig{},
	&AdminLog{},
	&AdminAdminuser{},
	&AdminBaseconfig{},
	&AdminRole{},
	&AdminCrontab{},
	&AdminMembe{},
	&AdminGoodsCata{},
	&AdminSupplier{},
	&AdminGoods{},
	&AdminLinkcata{},
	&AdminLink{},
	&AdminTest{},
	&AdminCar{},
	&AdminNode{},
}
