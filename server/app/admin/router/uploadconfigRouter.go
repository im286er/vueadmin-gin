package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminUploadconfig struct{}

/**
 * @description(缩略图配置路由)
 * @buildcode(true)
 */
func (AdminUploadconfig) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Uploadconfig/index", controller.AdminUploadconfig.GetPageList)           //数据列表
		r.POST("/admin/Uploadconfig/updateExt", controller.AdminUploadconfig.UpdateExt)         //修改排序开关
		r.POST("/admin/Uploadconfig/add", controller.AdminUploadconfig.Add)                     //添加
		r.POST("/admin/Uploadconfig/update", controller.AdminUploadconfig.Update)               //修改
		r.POST("/admin/Uploadconfig/getUpdateInfo", controller.AdminUploadconfig.GetUpdateInfo) //修改
		r.POST("/admin/Uploadconfig/delete", controller.AdminUploadconfig.Delete)               //删除
		r.POST("/admin/Uploadconfig/detail", controller.AdminUploadconfig.Detail)               //查看详情
	}
}
