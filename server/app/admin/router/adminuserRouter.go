package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminAdminuser struct{}

/**
 * @description(用户管理路由)
 * @buildcode(true)
 */
func (AdminAdminuser) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Adminuser/index", controller.AdminAdminuser.GetPageList)           //数据列表
		r.POST("/admin/Adminuser/updateExt", controller.AdminAdminuser.UpdateExt)         //修改排序开关
		r.POST("/admin/Adminuser/add", controller.AdminAdminuser.Add)                     //添加
		r.POST("/admin/Adminuser/update", controller.AdminAdminuser.Update)               //修改
		r.POST("/admin/Adminuser/getUpdateInfo", controller.AdminAdminuser.GetUpdateInfo) //修改
		r.POST("/admin/Adminuser/delete", controller.AdminAdminuser.Delete)               //删除
		r.POST("/admin/Adminuser/detail", controller.AdminAdminuser.Detail)               //查看详情
		r.POST("/admin/Adminuser/resetPwd", controller.AdminAdminuser.ResetPwd)           //重置密码
		r.POST("/admin/Adminuser/getFieldList", controller.AdminAdminuser.GetFieldList)   //查询列表
	}
}
