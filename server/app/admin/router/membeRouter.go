package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminMembe struct{}

/**
 * @description(会员管理路由)
 * @buildcode(true)
 */
func (AdminMembe) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Membe/index", controller.AdminMembe.GetPageList)           //数据列表
		r.POST("/admin/Membe/updateExt", controller.AdminMembe.UpdateExt)         //修改排序开关
		r.POST("/admin/Membe/add", controller.AdminMembe.Add)                     //添加
		r.POST("/admin/Membe/update", controller.AdminMembe.Update)               //修改
		r.POST("/admin/Membe/getUpdateInfo", controller.AdminMembe.GetUpdateInfo) //修改
		r.POST("/admin/Membe/delete", controller.AdminMembe.Delete)               //删除
		r.POST("/admin/Membe/detail", controller.AdminMembe.Detail)               //查看详情
		r.POST("/admin/Membe/resetPwd", controller.AdminMembe.ResetPwd)           //重置密码
		r.POST("/admin/Membe/import", controller.AdminMembe.Import)               //导入
		r.POST("/admin/Membe/export", controller.AdminMembe.Export)               //客户端导出
		r.POST("/admin/Membe/exportServer", controller.AdminMembe.ExportServer)   //服务端导出
		r.POST("/admin/Membe/jia", controller.AdminMembe.Jia)                     //充值
		r.POST("/admin/Membe/jian", controller.AdminMembe.Jian)                   //扣除
		r.POST("/admin/Membe/forbidden", controller.AdminMembe.Forbidden)         //状态禁用
	}
}
