package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
	"github.com/vueadmin/pkg/upload"
)

type AdminBase struct{}

func (AdminBase) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Base/getSiteInfo", middleware.BeforAction(), controller.AdminBase.GetSiteInfo) //获取站点信息，左侧菜单栏，vue组件信息
		r.POST("/admin/Index/main", controller.AdminIndex.Main)                                       //后台首页接口
		r.POST("/admin/Base/resetPwd", controller.AdminBase.ResetPwd)                                 //重置密码
		r.POST("/admin/Base/getShenshiqu", controller.AdminBase.GetShenshiqu)                         //获取省市区信息
		r.POST("/admin/Upload/upload", middleware.BeforAction(), upload.Upload.Upload)                //文件上传
		r.POST("/admin/Base/fileList", controller.AdminFile.GetPageList)                              //文件列表
		r.POST("/admin/Base/deleteFile", controller.AdminFile.DeleteFile)                             //删除物理文件
		r.POST("/admin/Upload/createFile", controller.AdminFile.Add)                                  //oss上传文件写入库\
		r.POST("/admin/Base/clearCache", controller.AdminBase.ClearCache)                             //清除缓存（内存缓存）
		r.POST("/admin/Base/getRoleMenus", controller.AdminBase.GetRoleMenus)                         //获取权限节点
	}
}
