package router

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/controller"
	"github.com/vueadmin/middleware"
)

type AdminRole struct{}

/**
 * @description(角色管理路由)
 * @buildcode(true)
 */
func (AdminRole) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.POST("/admin/Role/index", controller.AdminRole.GetPageList)           //数据列表
		r.POST("/admin/Role/updateExt", controller.AdminRole.UpdateExt)         //修改排序开关
		r.POST("/admin/Role/add", controller.AdminRole.Add)                     //添加
		r.POST("/admin/Role/update", controller.AdminRole.Update)               //修改
		r.POST("/admin/Role/getUpdateInfo", controller.AdminRole.GetUpdateInfo) //修改
		r.POST("/admin/Role/delete", controller.AdminRole.Delete)               //删除
		r.POST("/admin/Role/detail", controller.AdminRole.Detail)               //查看详情
	}
}
