package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/service"
	"github.com/vueadmin/global"
	"github.com/vueadmin/pkg/cache"
	"github.com/vueadmin/utils"
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/common/result"
	"github.com/vueadmin/utils/conv"
	"github.com/vueadmin/utils/response"
	"math"
	"net/http"
)

var AdminLog = new(adminLog)

type adminLog struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (adminLog) GetPageList(c *gin.Context) {
	req := dto.AdminLogPageReq{
		PageReq: request.PageReq{Page: 1, Limit: 20},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, count, err := service.AdminLogService.GetPageList(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	var sqldata map[string]interface{}
	if req.Page == 1 {
		sqldata, err = service.AdminLogService.GetFieldList(c)
		if err != nil {
			response.Err(c, err)
			return
		}
	}
	response.Success(c, "返回成功", result.M{
		"data":           list,
		"total":          count,
		"sql_field_data": sqldata,
	})
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (adminLog) Delete(c *gin.Context) {
	req := dto.AdminLogDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminLogService.Delete(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "删除成功", nil)
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (adminLog) Detail(c *gin.Context) {
	req := dto.AdminLogDetailReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.AdminLogService.Detail(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(导出)
 * @buildcode(true)
 */
func (adminLog) Export(c *gin.Context) {
	req := dto.AdminLogPageReq{
		PageReq: request.PageReq{Page: 1, Limit: 200},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, count, err := service.AdminLogService.Export(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	totalPage := math.Ceil(conv.Float64(count) / conv.Float64(req.Limit))

	data := make(map[string]interface{})
	data["filename"] = fmt.Sprintf("日志管理.%s", global.CONF.Export)
	data["percentage"] = math.Ceil(conv.Float64(req.Page*100) / conv.Float64(totalPage))
	data["header"] = conv.StringToSlice("所属应用,日志类型,用户名,请求url,客户端ip,浏览器信息,请求内容,异常信息,创建时间", ",")
	data["data"] = list
	data["status"] = 200

	if req.Page >= conv.Int(totalPage) {
		cache.Gocache.Delete("log_count")
	}
	c.JSON(http.StatusOK, data)
}

/**
 * @description(查询sql下拉列表)
 * @buildcode(true)
 */
func (adminLog) GetFieldList(c *gin.Context) {
	data, err := service.AdminLogService.GetFieldList(c)
	if err != nil {
		response.Fail(c, err.Error())
		return
	}
	response.Success(c, "返回成功", data)
}
