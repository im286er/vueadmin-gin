package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/service"
	"github.com/vueadmin/utils/response"
)

var AdminBaseconfig = new(adminBaseconfig)

type adminBaseconfig struct{}

/**
 * @description(配置表单)
 * @buildcode(true)
 */
func (adminBaseconfig) Index(c *gin.Context) {
	info, err := service.AdminBaseconfigService.Index()
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(更新)
 * @buildcode(true)
 */
func (adminBaseconfig) Update(c *gin.Context) {
	var req map[string]interface{}
	if err := c.ShouldBind(&req); err != nil {
		response.Err(c, err)
		return
	}
	err := service.AdminBaseconfigService.Update(req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}
