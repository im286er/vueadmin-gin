package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/service"
	"github.com/vueadmin/utils"
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/common/result"
	"github.com/vueadmin/utils/response"
)

var AdminLink = new(adminLink)

type adminLink struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (adminLink) GetPageList(c *gin.Context) {
	req := dto.AdminLinkPageReq{
		PageReq: request.PageReq{Page: 1, Limit: 20},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, count, err := service.AdminLinkService.GetPageList(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	var sqldata map[string]interface{}
	if req.Page == 1 {
		sqldata, err = service.AdminLinkService.GetFieldList(c)
		if err != nil {
			response.Err(c, err)
			return
		}
	}
	response.Success(c, "返回成功", result.M{
		"data":           list,
		"total":          count,
		"sql_field_data": sqldata,
	})
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (adminLink) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminLinkService.UpdateExt(req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (adminLink) Add(c *gin.Context) {
	req := dto.AdminLinkAddReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	ret, err := service.AdminLinkService.Add(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", ret)
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (adminLink) Update(c *gin.Context) {
	req := dto.AdminLinkUpdateReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminLinkService.Update(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (adminLink) GetUpdateInfo(c *gin.Context) {
	req := dto.AdminLinkGetUpdateInfoReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.AdminLinkService.GetUpdateInfo(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (adminLink) Delete(c *gin.Context) {
	req := dto.AdminLinkDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminLinkService.Delete(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "删除成功", nil)
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (adminLink) Detail(c *gin.Context) {
	req := dto.AdminLinkDetailReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.AdminLinkService.Detail(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(查询sql下拉列表)
 * @buildcode(true)
 */
func (adminLink) GetFieldList(c *gin.Context) {
	data, err := service.AdminLinkService.GetFieldList(c)
	if err != nil {
		response.Fail(c, err.Error())
		return
	}
	response.Success(c, "返回成功", data)
}
