package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/service"
	"github.com/vueadmin/utils"
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/common/result"
	"github.com/vueadmin/utils/response"
)

var AdminAdminuser = new(adminAdminuser)

type adminAdminuser struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (adminAdminuser) GetPageList(c *gin.Context) {
	req := dto.AdminAdminuserPageReq{
		PageReq: request.PageReq{Page: 1, Limit: 20},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, count, err := service.AdminAdminuserService.GetPageList(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	var sqldata map[string]interface{}
	if req.Page == 1 {
		sqldata, err = service.AdminAdminuserService.GetFieldList(c)
		if err != nil {
			response.Err(c, err)
			return
		}
	}
	response.Success(c, "返回成功", result.M{
		"data":           list,
		"total":          count,
		"sql_field_data": sqldata,
	})
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (adminAdminuser) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminAdminuserService.UpdateExt(req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (adminAdminuser) Add(c *gin.Context) {
	req := dto.AdminAdminuserAddReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	ret, err := service.AdminAdminuserService.Add(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", ret)
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (adminAdminuser) Update(c *gin.Context) {
	req := dto.AdminAdminuserUpdateReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminAdminuserService.Update(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (adminAdminuser) GetUpdateInfo(c *gin.Context) {
	req := dto.AdminAdminuserGetUpdateInfoReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.AdminAdminuserService.GetUpdateInfo(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (adminAdminuser) Delete(c *gin.Context) {
	req := dto.AdminAdminuserDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminAdminuserService.Delete(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "删除成功", nil)
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (adminAdminuser) Detail(c *gin.Context) {
	req := dto.AdminAdminuserDetailReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.AdminAdminuserService.Detail(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(重置密码)
 * @buildcode(true)
 */
func (adminAdminuser) ResetPwd(c *gin.Context) {
	req := dto.AdminAdminuserResetPwdReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminAdminuserService.ResetPwd(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(查询sql下拉列表)
 * @buildcode(true)
 */
func (adminAdminuser) GetFieldList(c *gin.Context) {
	data, err := service.AdminAdminuserService.GetFieldList(c)
	if err != nil {
		response.Fail(c, err.Error())
		return
	}
	response.Success(c, "返回成功", data)
}
