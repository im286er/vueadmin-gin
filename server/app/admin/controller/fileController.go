package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/service"
	"github.com/vueadmin/utils"
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/common/result"
	"github.com/vueadmin/utils/response"
)

var AdminFile = new(adminFile)

type adminFile struct{}

// 数据列表
func (adminFile) GetPageList(c *gin.Context) {
	req := dto.AdminFilePageReq{
		PageReq: request.PageReq{Page: 1, Limit: 20},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, count, err := service.AdminFileService.GetPageList(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", result.M{
		"data":  list,
		"total": count,
	})
}

// 创建
func (adminFile) Add(c *gin.Context) {
	req := dto.AdminFileAddReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	ret, err := service.AdminFileService.Add(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "添加成功", ret)
}

// 删除
func (adminFile) Delete(c *gin.Context) {
	req := dto.AdminFileDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminFileService.Delete(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "删除成功", nil)
}

// 删除物理文件
func (adminFile) DeleteFile(c *gin.Context) {
	req := dto.AdminFileDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminFileService.Delete(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "删除成功", nil)
}
