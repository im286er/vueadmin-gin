package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/service"
	"github.com/vueadmin/pkg/cache"
	"github.com/vueadmin/utils"
	"github.com/vueadmin/utils/conv"
	"github.com/vueadmin/utils/response"
	"net/http"
)

var AdminBase = new(adminBase)

type adminBase struct{}

// 获取菜单信息以及vue组件信息
func (adminBase) GetSiteInfo(c *gin.Context) {
	data := make(map[string]interface{})
	data["site_title"] = c.MustGet("site_title")
	data["logo"] = c.MustGet("logo")
	data["role_id"] = utils.GetClaims("RoleId", c)
	data["user"] = utils.GetClaims("User", c)

	menu := service.AdminBaseService.GetMenu()
	baseComponents := service.AdminBaseService.GetComponents()

	c.JSON(http.StatusOK, gin.H{
		"status":     http.StatusOK,
		"menu":       menu,
		"data":       data,
		"components": baseComponents,
		"actions":    conv.StringToSlice(conv.String(utils.GetClaims("Access", c)), ","),
	})
}

// 获取权限节点
func (adminBase) GetRoleMenus(c *gin.Context) {

}

// 重置密码
func (adminBase) ResetPwd(c *gin.Context) {
	req := dto.AdminResetPwdReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminBaseService.ResetPwd(&req, utils.GetClaims("UserId", c))
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "重置成功", nil)
}

// 清除内存缓存
func (adminBase) ClearCache(c *gin.Context) {
	cache.New().Delete("base_config")
	response.Success(c, "操作成功", nil)
}

// 获取省市区数据
func (adminBase) GetShenshiqu(c *gin.Context) {
	req := dto.Shenshiqu{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	content := utils.FileGetContents("public/assets/shengshiqu/city-data-level" + conv.String(req.Type) + ".json")
	c.String(http.StatusOK, content)
}
