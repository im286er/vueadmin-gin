package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/service"
	"github.com/vueadmin/utils"
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/common/result"
	"github.com/vueadmin/utils/response"
)

var AdminGoods = new(adminGoods)

type adminGoods struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (adminGoods) GetPageList(c *gin.Context) {
	req := dto.AdminGoodsPageReq{
		PageReq: request.PageReq{Page: 1, Limit: 20},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, count, err := service.AdminGoodsService.GetPageList(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	var sqldata map[string]interface{}
	if req.Page == 1 {
		sqldata, err = service.AdminGoodsService.GetFieldList(c)
		if err != nil {
			response.Err(c, err)
			return
		}
	}
	response.Success(c, "返回成功", result.M{
		"data":           list,
		"total":          count,
		"sql_field_data": sqldata,
	})
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (adminGoods) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminGoodsService.UpdateExt(req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (adminGoods) Add(c *gin.Context) {
	req := dto.AdminGoodsAddReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	ret, err := service.AdminGoodsService.Add(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", ret)
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (adminGoods) Update(c *gin.Context) {
	req := dto.AdminGoodsUpdateReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminGoodsService.Update(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (adminGoods) GetUpdateInfo(c *gin.Context) {
	req := dto.AdminGoodsGetUpdateInfoReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.AdminGoodsService.GetUpdateInfo(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (adminGoods) Delete(c *gin.Context) {
	req := dto.AdminGoodsDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminGoodsService.Delete(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "删除成功", nil)
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (adminGoods) Detail(c *gin.Context) {
	req := dto.AdminGoodsDetailReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.AdminGoodsService.Detail(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(查询sql下拉列表)
 * @buildcode(true)
 */
func (adminGoods) GetFieldList(c *gin.Context) {
	data, err := service.AdminGoodsService.GetFieldList(c)
	if err != nil {
		response.Fail(c, err.Error())
		return
	}
	response.Success(c, "返回成功", data)
}

/**
 * @description(所属分类联动)
 * @buildcode(true)
 */
func (adminGoods) GetClassIdList(c *gin.Context) {
	req := dto.AdminGoodsClassIdReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	data, err := service.AdminGoodsService.GetClassIdList(&req)
	if err != nil {
		response.Fail(c, err.Error())
		return
	}
	response.Success(c, "返回成功", data)
}
