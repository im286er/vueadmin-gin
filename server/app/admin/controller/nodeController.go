package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/service"
	"github.com/vueadmin/utils"
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/common/result"
	"github.com/vueadmin/utils/response"
)

var AdminNode = new(adminNode)

type adminNode struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (adminNode) GetPageList(c *gin.Context) {
	req := dto.AdminNodePageReq{
		PageReq: request.PageReq{Page: 1, Limit: 100},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, count, err := service.AdminNodeService.GetPageList(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	var sqldata map[string]interface{}
	if req.Page == 1 {
		sqldata, err = service.AdminNodeService.GetFieldList(c)
		if err != nil {
			response.Err(c, err)
			return
		}
	}
	response.Success(c, "返回成功", result.M{
		"data":           list,
		"total":          count,
		"sql_field_data": sqldata,
	})
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (adminNode) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminNodeService.UpdateExt(req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (adminNode) Add(c *gin.Context) {
	req := dto.AdminNodeAddReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	ret, err := service.AdminNodeService.Add(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", ret)
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (adminNode) Update(c *gin.Context) {
	req := dto.AdminNodeUpdateReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminNodeService.Update(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (adminNode) GetUpdateInfo(c *gin.Context) {
	req := dto.AdminNodeGetUpdateInfoReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.AdminNodeService.GetUpdateInfo(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (adminNode) Delete(c *gin.Context) {
	req := dto.AdminNodeDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminNodeService.Delete(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "删除成功", nil)
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (adminNode) Detail(c *gin.Context) {
	req := dto.AdminNodeDetailReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.AdminNodeService.Detail(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(查询sql下拉列表)
 * @buildcode(true)
 */
func (adminNode) GetFieldList(c *gin.Context) {
	data, err := service.AdminNodeService.GetFieldList(c)
	if err != nil {
		response.Fail(c, err.Error())
		return
	}
	response.Success(c, "返回成功", data)
}
