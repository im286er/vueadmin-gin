package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/service"
	"github.com/vueadmin/global"
	"github.com/vueadmin/pkg/cache"
	"github.com/vueadmin/utils"
	"github.com/vueadmin/utils/common/request"
	"github.com/vueadmin/utils/common/result"
	"github.com/vueadmin/utils/conv"
	"github.com/vueadmin/utils/response"
	"github.com/xuri/excelize/v2"
	"math"
	"net/http"
)

var AdminMembe = new(adminMembe)

type adminMembe struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (adminMembe) GetPageList(c *gin.Context) {
	req := dto.AdminMembePageReq{
		PageReq: request.PageReq{Page: 1, Limit: 20},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, count, err := service.AdminMembeService.GetPageList(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", result.M{
		"data":       list,
		"total":      count,
		"sum_amount": service.AdminMembeService.GetAmountSum(&req),
	})
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (adminMembe) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminMembeService.UpdateExt(req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (adminMembe) Add(c *gin.Context) {
	req := dto.AdminMembeAddReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	ret, err := service.AdminMembeService.Add(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", ret)
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (adminMembe) Update(c *gin.Context) {
	req := dto.AdminMembeUpdateReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminMembeService.Update(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (adminMembe) GetUpdateInfo(c *gin.Context) {
	req := dto.AdminMembeGetUpdateInfoReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.AdminMembeService.GetUpdateInfo(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (adminMembe) Delete(c *gin.Context) {
	req := dto.AdminMembeDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminMembeService.Delete(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "删除成功", nil)
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (adminMembe) Detail(c *gin.Context) {
	req := dto.AdminMembeDetailReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, err := service.AdminMembeService.Detail(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

/**
 * @description(重置密码)
 * @buildcode(true)
 */
func (adminMembe) ResetPwd(c *gin.Context) {
	req := dto.AdminMembeResetPwdReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminMembeService.ResetPwd(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}

/**
 * @description(导入)
 * @buildcode(true)
 */
func (adminMembe) Import(c *gin.Context) {
	req := []dto.AdminMembeImportReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminMembeService.Import(req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "导入成功", nil)
}

/**
 * @description(客户端导出)
 * @buildcode(true)
 */
func (adminMembe) Export(c *gin.Context) {
	req := dto.AdminMembePageReq{
		PageReq: request.PageReq{Page: 1, Limit: 200},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, count, err := service.AdminMembeService.Export(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	totalPage := math.Ceil(conv.Float64(count) / conv.Float64(req.Limit))

	data := make(map[string]interface{})
	data["filename"] = fmt.Sprintf("会员管理.%s", global.CONF.Export)
	data["percentage"] = math.Ceil(conv.Float64(req.Page*100) / conv.Float64(totalPage))
	data["header"] = conv.StringToSlice("用户名,性别,手机号,头像,邮箱,密码,积分,状态,省市区,创建时间", ",")
	data["data"] = list
	data["status"] = 200

	if req.Page >= conv.Int(totalPage) {
		cache.Gocache.Delete("membe_count")
	}
	c.JSON(http.StatusOK, data)
}

/**
 * @description(服务端导出)
 * @buildcode(true)
 */
func (adminMembe) ExportServer(c *gin.Context) {
	req := dto.AdminMembePageReq{
		PageReq: request.PageReq{Page: 1, Limit: 200},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, count, err := service.AdminMembeService.ExportServer(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	totalPage := math.Ceil(conv.Float64(count) / conv.Float64(req.Limit))

	data := make(map[string]interface{})
	if req.Page < conv.Int(totalPage) {
		data["percentage"] = math.Ceil(conv.Float64(req.Page*100) / conv.Float64(totalPage))
		data["status"] = 200
		c.JSON(http.StatusOK, data)
		return
	}

	file := excelize.NewFile()
	file.SetCellValue("Sheet1", "A1", "用户名")
	file.SetCellValue("Sheet1", "B1", "性别")
	file.SetCellValue("Sheet1", "C1", "手机号")
	file.SetCellValue("Sheet1", "D1", "头像")
	file.SetCellValue("Sheet1", "E1", "邮箱")
	file.SetCellValue("Sheet1", "F1", "密码")
	file.SetCellValue("Sheet1", "G1", "积分")
	file.SetCellValue("Sheet1", "H1", "状态")
	file.SetCellValue("Sheet1", "I1", "省市区")
	file.SetCellValue("Sheet1", "J1", "创建时间")

	for k, v := range list {
		file.SetCellValue("Sheet1", fmt.Sprintf("A%d", k+2), conv.String(v.Username))
		file.SetCellValue("Sheet1", fmt.Sprintf("B%d", k+2), conv.String(v.Sex))
		file.SetCellValue("Sheet1", fmt.Sprintf("C%d", k+2), conv.String(v.Mobile))
		file.SetCellValue("Sheet1", fmt.Sprintf("D%d", k+2), conv.String(v.Pic))
		file.SetCellValue("Sheet1", fmt.Sprintf("E%d", k+2), conv.String(v.Email))
		file.SetCellValue("Sheet1", fmt.Sprintf("F%d", k+2), conv.String(v.Password))
		file.SetCellValue("Sheet1", fmt.Sprintf("G%d", k+2), conv.String(v.Amount))
		file.SetCellValue("Sheet1", fmt.Sprintf("H%d", k+2), conv.String(v.Status))
		file.SetCellValue("Sheet1", fmt.Sprintf("I%d", k+2), conv.String(v.Ssq))
		file.SetCellValue("Sheet1", fmt.Sprintf("J%d", k+2), conv.String(v.CreateTime))
	}

	filename := fmt.Sprintf("/uploads/excel/会员管理.%s", global.CONF.Export)
	file.SaveAs(fmt.Sprintf("./public/%s", filename))

	cache.Gocache.Delete("membe")
	cache.Gocache.Delete("membe_count")

	response.Success(c, "返回成功", result.M{
		"filepath": filename,
	})
}

/**
 * @description(充值)
 * @buildcode(true)
 */
func (adminMembe) Jia(c *gin.Context) {
	req := dto.AdminMembeJiaReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminMembeService.Jia(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "操作成功", nil)
}

/**
 * @description(扣除)
 * @buildcode(true)
 */
func (adminMembe) Jian(c *gin.Context) {
	req := dto.AdminMembeJianReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminMembeService.Jian(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "操作成功", nil)
}

/**
 * @description(状态禁用)
 * @buildcode(true)
 */
func (adminMembe) Forbidden(c *gin.Context) {
	req := dto.AdminMembeForbiddenReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	err := service.AdminMembeService.Forbidden(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "修改成功", nil)
}
