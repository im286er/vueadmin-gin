package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/utils/response"
	"math/rand"
	"time"
)

var AdminIndex = new(adminIndex)

type adminIndex struct{}

// 获取菜单信息以及vue组件信息
func (adminIndex) Main(c *gin.Context) {
	data := make(map[string]interface{})
	data["status"] = 200
	data["echat_data"] = map[string]interface{}{
		"day_count": map[string]interface{}{
			"title": "当月业绩折线图",
			"day":   []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30},
			"data":  []int{0, 0, 0, 0, 0, 126, 246, 452, 45, 36, 479, 588, 434, 9, 18, 27, 18, 88, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		},
	}

	data["card_data"] = getCardData()
	response.Success(c, "返回成功", data)
}

// 卡片数据
func getCardData() []map[string]interface{} {
	rand.Seed(time.Now().UnixNano())
	var data = []map[string]interface{}{
		{
			"title_icon":            "el-icon-user",
			"card_title":            "访问",
			"card_cycle":            "年",
			"card_cycle_back_color": "#409EFF",
			"bottom_title":          "访问总量",
			"vist_num":              rand.Intn(10000),
			"vist_all_num":          rand.Intn(10000),
			"vist_all_icon":         "el-icon-trophy",
		},
		{
			"title_icon":            "el-icon-download",
			"card_title":            "下载",
			"card_cycle":            "月",
			"card_cycle_back_color": "#67C23A",
			"bottom_title":          "下载总量",
			"vist_num":              rand.Intn(10000),
			"vist_all_num":          rand.Intn(10000),
			"vist_all_icon":         "el-icon-download",
		},
		{
			"title_icon":            "el-icon-wallet",
			"card_title":            "收入",
			"card_cycle":            "日",
			"card_cycle_back_color": "#F56C6C",
			"bottom_title":          "总收入",
			"vist_num":              rand.Intn(10000),
			"vist_all_num":          rand.Intn(10000),
			"vist_all_icon":         "el-icon-wallet",
		},
		{
			"title_icon":            "el-icon-coordinate",
			"card_title":            "用户",
			"card_cycle":            "月",
			"card_cycle_back_color": "#E6A23C",
			"bottom_title":          "总用户",
			"vist_num":              rand.Intn(10000),
			"vist_all_num":          rand.Intn(10000),
			"vist_all_icon":         "el-icon-coordinate",
		},
	}
	return data
}

// 退出登录
func (adminIndex) Logout(c *gin.Context) {
	response.Success(c, "退出登录", nil)
}
