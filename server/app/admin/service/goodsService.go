package service

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/app/admin/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
	"gorm.io/gorm"
)

var AdminGoodsService = new(adminGoods)

type adminGoods struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p adminGoods) GetPageList(req *dto.AdminGoodsPageReq) ([]*vo.AdminGoodsList, int64, error) {
	var (
		entity model.AdminGoods
		list   []*vo.AdminGoodsList
		count  int64
	)
	query := global.DB.Model(&entity)
	if !conv.IsEmpty(req.GoodsId) {
		query = query.Where("goods_id = ?", req.GoodsId)
	}
	if !conv.IsEmpty(req.GoodsName) {
		query = query.Where("goods_name LIKE ?", "%"+req.GoodsName+"%")
	}
	if !conv.IsEmpty(req.SupplierId) {
		query = query.Where("supplier_id = ?", req.SupplierId)
	}
	if !conv.IsEmpty(req.ClassId) {
		query = query.Where("class_id = ?", req.ClassId)
	}
	if !conv.IsEmpty(req.Status) {
		query = query.Where("status = ?", req.Status)
	}
	if !conv.IsEmpty(req.CreateTime) {
		query = query.Where("create_time BETWEEN ? AND ?", conv.UnixTime(req.CreateTime[0]), conv.UnixTime(req.CreateTime[1]))
	}
	order := "goods_id desc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	query = query.Preload("GoodsCata", func(db *gorm.DB) *gorm.DB {
		return db.Select("class_id,class_name")
	})
	query = query.Preload("Supplier", func(db *gorm.DB) *gorm.DB {
		return db.Select("supplier_id,supplier_name")
	})
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)

	return list, count, nil
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (p adminGoods) UpdateExt(req map[string]interface{}) error {
	entity := model.AdminGoods{}
	if err := global.DB.Model(&entity).Where("goods_id", req["goods_id"]).Updates(req).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (p adminGoods) Add(req *dto.AdminGoodsAddReq) (uint, error) {
	entity := model.AdminGoods{}
	entity.GoodsName = req.GoodsName
	entity.SupplierId = req.SupplierId
	entity.ClassId = req.ClassId
	entity.Pic = req.Pic
	entity.SalePrice = req.SalePrice
	entity.Images = req.Images
	entity.Status = req.Status
	entity.Cd = req.Cd
	entity.Store = req.Store
	entity.Sortid = req.Sortid
	entity.Detail = req.Detail
	entity.CreateTime = conv.NowUnix()

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	if entity.Sortid == 0 {
		if err := global.DB.Model(&entity).Update("sortid", entity.GoodsId).Error; err != nil {
			return 0, err
		}
	}
	return entity.GoodsId, nil
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (p adminGoods) Update(req *dto.AdminGoodsUpdateReq) error {
	entity := model.AdminGoods{
		GoodsId: req.GoodsId,
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	entity.GoodsName = req.GoodsName
	entity.SupplierId = req.SupplierId
	entity.ClassId = req.ClassId
	entity.Pic = req.Pic
	entity.SalePrice = req.SalePrice
	entity.Images = req.Images
	entity.Status = req.Status
	entity.Cd = req.Cd
	entity.Store = req.Store
	entity.Sortid = req.Sortid
	entity.Detail = req.Detail
	entity.CreateTime = conv.UnixTime(req.CreateTime)

	field := conv.StringToSlice("goods_name,supplier_id,class_id,pic,sale_price,images,status,cd,store,sortid,detail,create_time", ",")

	if err := global.DB.Select(field).Updates(&entity).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (p adminGoods) GetUpdateInfo(req *dto.AdminGoodsGetUpdateInfoReq) (*model.AdminGoods, error) {
	entity := model.AdminGoods{}
	if err := global.DB.Where("goods_id = ?", req.GoodsId).Limit(1).First(&entity).Error; err != nil {
		return nil, err
	}
	return &entity, nil
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (p adminGoods) Delete(req *dto.AdminGoodsDeleteReq) error {
	query := global.DB.Where("goods_id IN ?", conv.Slice(req.GoodsId))
	if err := query.Delete(&model.AdminGoods{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p adminGoods) Detail(req *dto.AdminGoodsDetailReq) (*vo.AdminGoodsDetail, error) {
	var info vo.AdminGoodsDetail
	query := global.DB.Model(&model.AdminGoods{})
	query = query.Preload("GoodsCata", func(db *gorm.DB) *gorm.DB {
		return db.Select("class_id,class_name")
	})
	query = query.Preload("Supplier", func(db *gorm.DB) *gorm.DB {
		return db.Select("supplier_id,supplier_name")
	})
	if err := query.Where("goods_id = ?", req.GoodsId).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}

/**
 * @description(查询sql下拉字段列表)
 * @buildcode(true)
 */
func (p adminGoods) GetFieldList(c *gin.Context) (map[string]interface{}, error) {
	var (
		SupplierId []*vo.AdminGoodsSupplierId
	)
	if err := global.DB.Raw(`select supplier_id,supplier_name from cd_supplier`).Scan(&SupplierId).Error; err != nil {
		return nil, err
	}
	data := make(map[string]interface{})
	data["supplier_ids"] = SupplierId
	return data, nil
}

/**
 * @description(查询所属分类联动)
 * @buildcode(true)
 */
func (p adminGoods) GetClassIdList(req *dto.AdminGoodsClassIdReq) ([]*vo.AdminGoodsClassId, error) {
	var ClassId []*vo.AdminGoodsClassId
	sql := fmt.Sprintf(`select class_id,class_name,pid from cd_goods_cata where supplier_id = %d`, req.SupplierId)
	if err := global.DB.Raw(sql).Scan(&ClassId).Error; err != nil {
		return nil, err
	}
	return p.getTreeClassId(ClassId, 0), nil
}

/**
 * @description(格式所属分类树形结构)
 * @buildcode(true)
 */
func (p adminGoods) getTreeClassId(list []*vo.AdminGoodsClassId, pid int) []*vo.AdminGoodsClassId {
	res := make([]*vo.AdminGoodsClassId, 0)
	for _, v := range list {
		if v.Pid == pid {
			v.Children = p.getTreeClassId(list, v.ClassId)
			res = append(res, v)
		}
	}
	return res
}
