package service

import (
	"fmt"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/app/admin/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

var AdminCarService = new(adminCar)

type adminCar struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p adminCar) GetPageList(req *dto.AdminCarPageReq) ([]*vo.AdminCarList, int64, error) {
	var (
		entity model.AdminCar
		list   []*vo.AdminCarList
		count  int64
	)
	query := global.DB.Model(&entity)
	if !conv.IsEmpty(req.CarId) {
		query = query.Where("car_id = ?", req.CarId)
	}
	if !conv.IsEmpty(req.MembeId) {
		query = query.Where("membe_id = ?", req.MembeId)
	}
	order := "car_id desc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)

	return list, count, nil
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (p adminCar) UpdateExt(req map[string]interface{}) error {
	entity := model.AdminCar{}
	if err := global.DB.Model(&entity).Where("car_id", req["car_id"]).Updates(req).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (p adminCar) Add(req *dto.AdminCarAddReq) (uint, error) {
	entity := model.AdminCar{}
	entity.MembeId = req.MembeId
	entity.CarNo = req.CarNo
	entity.CreateTime = conv.NowUnix()

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	return entity.CarId, nil
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (p adminCar) Update(req *dto.AdminCarUpdateReq) error {
	entity := model.AdminCar{
		CarId: req.CarId,
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	entity.MembeId = req.MembeId
	entity.CarNo = req.CarNo
	entity.CreateTime = conv.UnixTime(req.CreateTime)

	field := conv.StringToSlice("membe_id,car_no,create_time", ",")

	if err := global.DB.Select(field).Updates(&entity).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (p adminCar) GetUpdateInfo(req *dto.AdminCarGetUpdateInfoReq) (*model.AdminCar, error) {
	entity := model.AdminCar{}
	if err := global.DB.Where("car_id = ?", req.CarId).Limit(1).First(&entity).Error; err != nil {
		return nil, err
	}
	return &entity, nil
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (p adminCar) Delete(req *dto.AdminCarDeleteReq) error {
	query := global.DB.Where("car_id IN ?", conv.Slice(req.CarId))
	if err := query.Delete(&model.AdminCar{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p adminCar) Detail(req *dto.AdminCarDetailReq) (*vo.AdminCarDetail, error) {
	var info vo.AdminCarDetail
	query := global.DB.Model(&model.AdminCar{})
	if err := query.Where("car_id = ?", req.CarId).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}
