package service

import (
	"fmt"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/app/admin/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

var AdminSupplierService = new(adminSupplier)

type adminSupplier struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p adminSupplier) GetPageList(req *dto.AdminSupplierPageReq) ([]*vo.AdminSupplierList, int64, error) {
	var (
		entity model.AdminSupplier
		list   []*vo.AdminSupplierList
		count  int64
	)
	query := global.DB.Model(&entity)
	if !conv.IsEmpty(req.SupplierId) {
		query = query.Where("supplier_id = ?", req.SupplierId)
	}
	if !conv.IsEmpty(req.SupplierName) {
		query = query.Where("supplier_name = ?", req.SupplierName)
	}
	if !conv.IsEmpty(req.Status) {
		query = query.Where("status = ?", req.Status)
	}
	if !conv.IsEmpty(req.Username) {
		query = query.Where("username = ?", req.Username)
	}
	if !conv.IsEmpty(req.CreateTime) {
		query = query.Where("create_time BETWEEN ? AND ?", conv.UnixTime(req.CreateTime[0]), conv.UnixTime(req.CreateTime[1]))
	}
	order := "supplier_id desc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)

	return list, count, nil
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (p adminSupplier) UpdateExt(req map[string]interface{}) error {
	entity := model.AdminSupplier{}
	if err := global.DB.Model(&entity).Where("supplier_id", req["supplier_id"]).Updates(req).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (p adminSupplier) Add(req *dto.AdminSupplierAddReq) (uint, error) {
	entity := model.AdminSupplier{}
	entity.SupplierName = req.SupplierName
	entity.Status = req.Status
	entity.Username = req.Username
	entity.Password = conv.EncryptPassword(req.Password)
	entity.CreateTime = conv.NowUnix()

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	return entity.SupplierId, nil
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (p adminSupplier) Update(req *dto.AdminSupplierUpdateReq) error {
	entity := model.AdminSupplier{
		SupplierId: req.SupplierId,
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	entity.SupplierName = req.SupplierName
	entity.Status = req.Status
	entity.Username = req.Username
	entity.CreateTime = conv.UnixTime(req.CreateTime)

	field := conv.StringToSlice("supplier_name,status,username,create_time", ",")

	if err := global.DB.Select(field).Updates(&entity).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (p adminSupplier) GetUpdateInfo(req *dto.AdminSupplierGetUpdateInfoReq) (*model.AdminSupplier, error) {
	entity := model.AdminSupplier{}
	if err := global.DB.Where("supplier_id = ?", req.SupplierId).Limit(1).First(&entity).Error; err != nil {
		return nil, err
	}
	return &entity, nil
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (p adminSupplier) Delete(req *dto.AdminSupplierDeleteReq) error {
	query := global.DB.Where("supplier_id IN ?", conv.Slice(req.SupplierId))
	if err := query.Delete(&model.AdminSupplier{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p adminSupplier) Detail(req *dto.AdminSupplierDetailReq) (*vo.AdminSupplierDetail, error) {
	var info vo.AdminSupplierDetail
	query := global.DB.Model(&model.AdminSupplier{})
	if err := query.Where("supplier_id = ?", req.SupplierId).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}

/**
 * @description(重置密码)
 * @buildcode(true)
 */
func (adminSupplier) ResetPwd(req *dto.AdminSupplierResetPwdReq) error {
	entity := model.AdminSupplier{
		SupplierId: conv.Uint(req.SupplierId),
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	if err := global.DB.Model(&entity).Update("password", conv.EncryptPassword(req.Password)).Error; err != nil {
		return err
	}
	return nil
}
