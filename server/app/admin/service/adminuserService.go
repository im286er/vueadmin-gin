package service

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/hook"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/app/admin/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
	"gorm.io/gorm"
)

var AdminAdminuserService = new(adminAdminuser)

type adminAdminuser struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p adminAdminuser) GetPageList(req *dto.AdminAdminuserPageReq) ([]*vo.AdminAdminuserList, int64, error) {
	var (
		entity model.AdminAdminuser
		list   []*vo.AdminAdminuserList
		count  int64
	)
	query := global.DB.Model(&entity)
	if !conv.IsEmpty(req.UserId) {
		query = query.Where("user_id = ?", req.UserId)
	}
	if !conv.IsEmpty(req.User) {
		query = query.Where("user = ?", req.User)
	}
	if !conv.IsEmpty(req.RoleId) {
		query = query.Where("role_id = ?", req.RoleId)
	}
	if !conv.IsEmpty(req.Status) {
		query = query.Where("status = ?", req.Status)
	}
	order := "user_id desc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	query = query.Preload("Role", func(db *gorm.DB) *gorm.DB {
		return db.Select("role_id,name")
	})
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)

	return list, count, nil
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (p adminAdminuser) UpdateExt(req map[string]interface{}) error {
	entity := model.AdminAdminuser{}
	if err := global.DB.Model(&entity).Where("user_id", req["user_id"]).Updates(req).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (p adminAdminuser) Add(req *dto.AdminAdminuserAddReq) (uint, error) {
	entity := model.AdminAdminuser{}
	entity.Name = req.Name
	entity.User = req.User
	entity.Pwd = conv.EncryptPassword(req.Pwd)
	entity.RoleId = req.RoleId
	entity.Note = req.Note
	entity.Status = req.Status
	entity.CreateTime = conv.NowUnix()

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	return entity.UserId, nil
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (p adminAdminuser) Update(req *dto.AdminAdminuserUpdateReq) error {
	entity := model.AdminAdminuser{
		UserId: req.UserId,
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	entity.Name = req.Name
	entity.User = req.User
	entity.RoleId = req.RoleId
	entity.Note = req.Note
	entity.Status = req.Status
	entity.CreateTime = conv.UnixTime(req.CreateTime)

	field := conv.StringToSlice("name,user,role_id,note,status,create_time", ",")

	if err := global.DB.Select(field).Updates(&entity).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (p adminAdminuser) GetUpdateInfo(req *dto.AdminAdminuserGetUpdateInfoReq) (*model.AdminAdminuser, error) {
	entity := model.AdminAdminuser{}
	if err := global.DB.Where("user_id = ?", req.UserId).Limit(1).First(&entity).Error; err != nil {
		return nil, err
	}
	return &entity, nil
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (p adminAdminuser) Delete(req *dto.AdminAdminuserDeleteReq) error {
	if err := hook.BeforAdminAdminuserDeleteHook(req); err != nil {
		return errors.New(err.Error())
	}
	query := global.DB.Where("user_id IN ?", conv.Slice(req.UserId))
	if err := query.Delete(&model.AdminAdminuser{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p adminAdminuser) Detail(req *dto.AdminAdminuserDetailReq) (*vo.AdminAdminuserDetail, error) {
	var info vo.AdminAdminuserDetail
	query := global.DB.Model(&model.AdminAdminuser{})
	if err := query.Where("user_id = ?", req.UserId).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}

/**
 * @description(重置密码)
 * @buildcode(true)
 */
func (adminAdminuser) ResetPwd(req *dto.AdminAdminuserResetPwdReq) error {
	entity := model.AdminAdminuser{
		UserId: conv.Uint(req.UserId),
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	if err := global.DB.Model(&entity).Update("pwd", conv.EncryptPassword(req.Pwd)).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查询sql下拉字段列表)
 * @buildcode(true)
 */
func (p adminAdminuser) GetFieldList(c *gin.Context) (map[string]interface{}, error) {
	var (
		RoleId []*vo.AdminAdminuserRoleId
	)
	if err := global.DB.Raw(`select role_id,name from cd_role`).Scan(&RoleId).Error; err != nil {
		return nil, err
	}
	data := make(map[string]interface{})
	data["role_ids"] = RoleId
	return data, nil
}
