package service

import (
	"fmt"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/app/admin/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

var AdminTestService = new(adminTest)

type adminTest struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p adminTest) GetPageList(req *dto.AdminTestPageReq) ([]*vo.AdminTestList, int64, error) {
	var (
		entity model.AdminTest
		list   []*vo.AdminTestList
		count  int64
	)
	query := global.DB.Model(&entity)
	if !conv.IsEmpty(req.TestId) {
		query = query.Where("test_id = ?", req.TestId)
	}
	if !conv.IsEmpty(req.Title) {
		query = query.Where("title = ?", req.Title)
	}
	if !conv.IsEmpty(req.OnlyTrashed) {
		query = query.Unscoped().Where("deleted_at is not null")
	}
	order := "test_id desc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)

	return list, count, nil
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (p adminTest) UpdateExt(req map[string]interface{}) error {
	entity := model.AdminTest{}
	if err := global.DB.Model(&entity).Where("test_id", req["test_id"]).Updates(req).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (p adminTest) Add(req *dto.AdminTestAddReq) (uint, error) {
	entity := model.AdminTest{}
	entity.Title = req.Title
	entity.Status = req.Status

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	return entity.TestId, nil
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (p adminTest) Update(req *dto.AdminTestUpdateReq) error {
	entity := model.AdminTest{
		TestId: req.TestId,
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	entity.Title = req.Title
	entity.Status = req.Status

	field := conv.StringToSlice("title,status", ",")

	if err := global.DB.Select(field).Updates(&entity).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (p adminTest) GetUpdateInfo(req *dto.AdminTestGetUpdateInfoReq) (*model.AdminTest, error) {
	entity := model.AdminTest{}
	if err := global.DB.Where("test_id = ?", req.TestId).Limit(1).First(&entity).Error; err != nil {
		return nil, err
	}
	return &entity, nil
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (p adminTest) Delete(req *dto.AdminTestDeleteReq) error {
	query := global.DB.Where("test_id IN ?", conv.Slice(req.TestId))
	if req.OnlyTrashed {
		query = query.Unscoped()
	}
	if err := query.Delete(&model.AdminTest{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p adminTest) Detail(req *dto.AdminTestDetailReq) (*vo.AdminTestDetail, error) {
	var info vo.AdminTestDetail
	query := global.DB.Model(&model.AdminTest{})
	if err := query.Where("test_id = ?", req.TestId).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}

/**
 * @description(软删除回收站恢复数据)
 * @buildcode(true)
 */
func (p adminTest) Restore(req *dto.AdminTestRestoreReq) error {
	entity := model.AdminTest{}
	if err := global.DB.Model(&entity).Unscoped().Where("test_id IN ?", conv.Slice(req.TestId)).Update("deleted_at", nil).Error; err != nil {
		return err
	}
	return nil
}
