package service

import (
	"fmt"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/app/admin/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

var AdminLinkcataService = new(adminLinkcata)

type adminLinkcata struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p adminLinkcata) GetPageList(req *dto.AdminLinkcataPageReq) ([]*vo.AdminLinkcataList, int64, error) {
	var (
		list  []*vo.AdminLinkcataList
		count int64
	)
	where := "1=1 "
	if !conv.IsEmpty(req.LinkcataId) {
		where += fmt.Sprintf(`and a.linkcata_id = %d `, *req.LinkcataId)
	}
	if !conv.IsEmpty(req.ClassName) {
		where += fmt.Sprintf(`and a.class_name = "%s" `, req.ClassName)
	}
	if !conv.IsEmpty(req.Status) {
		where += fmt.Sprintf(`and a.status = %d `, *req.Status)
	}
	if !conv.IsEmpty(req.Count) {
		where += fmt.Sprintf(`and a.count = "%s" `, req.Count)
	}
	order := "linkcata_id desc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	skip := (req.Page - 1) * req.Limit
	sql := fmt.Sprintf(`select a.*,count(*) as count from cd_linkcata as a left join cd_link as b on a.linkcata_id = b.linkcata_id  where %s group by a.linkcata_id  order by %s limit %d,%d`, where, order, skip, req.Limit)
	if err := global.DB.Raw(sql).Scan(&list).Error; err != nil {
		return nil, 0, err
	}
	countSql := fmt.Sprintf(`select count(*) as count from (select a.*,count(*) as count from cd_linkcata as a left join cd_link as b on a.linkcata_id = b.linkcata_id  where %s group by a.linkcata_id ) as tmp`, where)
	global.DB.Raw(countSql).Scan(&count)

	return list, count, nil
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (p adminLinkcata) UpdateExt(req map[string]interface{}) error {
	entity := model.AdminLinkcata{}
	if err := global.DB.Model(&entity).Where("linkcata_id", req["linkcata_id"]).Updates(req).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (p adminLinkcata) Add(req *dto.AdminLinkcataAddReq) (uint, error) {
	entity := model.AdminLinkcata{}
	entity.ClassName = req.ClassName
	entity.Status = req.Status
	entity.Jdt = req.Jdt
	entity.Count = req.Count

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	return entity.LinkcataId, nil
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (p adminLinkcata) Update(req *dto.AdminLinkcataUpdateReq) error {
	entity := model.AdminLinkcata{
		LinkcataId: req.LinkcataId,
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	entity.ClassName = req.ClassName
	entity.Status = req.Status
	entity.Jdt = req.Jdt
	entity.Count = req.Count

	field := conv.StringToSlice("class_name,status,jdt,count", ",")

	if err := global.DB.Select(field).Updates(&entity).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (p adminLinkcata) GetUpdateInfo(req *dto.AdminLinkcataGetUpdateInfoReq) (*model.AdminLinkcata, error) {
	entity := model.AdminLinkcata{}
	if err := global.DB.Where("linkcata_id = ?", req.LinkcataId).Limit(1).First(&entity).Error; err != nil {
		return nil, err
	}
	return &entity, nil
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (p adminLinkcata) Delete(req *dto.AdminLinkcataDeleteReq) error {
	query := global.DB.Where("linkcata_id IN ?", conv.Slice(req.LinkcataId))
	if err := query.Delete(&model.AdminLinkcata{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p adminLinkcata) Detail(req *dto.AdminLinkcataDetailReq) (*vo.AdminLinkcataDetail, error) {
	var info vo.AdminLinkcataDetail
	query := global.DB.Model(&model.AdminLinkcata{})
	if err := query.Where("linkcata_id = ?", req.LinkcataId).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}
