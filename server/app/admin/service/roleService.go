package service

import (
	"errors"
	"fmt"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/app/admin/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

var AdminRoleService = new(adminRole)

type adminRole struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p adminRole) GetPageList(req *dto.AdminRolePageReq) ([]*vo.AdminRoleList, int64, error) {
	var (
		entity model.AdminRole
		list   []*vo.AdminRoleList
		count  int64
	)
	query := global.DB.Model(&entity)
	if !conv.IsEmpty(req.RoleId) {
		query = query.Where("role_id = ?", req.RoleId)
	}
	if !conv.IsEmpty(req.Name) {
		query = query.Where("name = ?", req.Name)
	}
	if !conv.IsEmpty(req.Status) {
		query = query.Where("status = ?", req.Status)
	}
	order := "role_id desc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)

	return list, count, nil
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (p adminRole) UpdateExt(req map[string]interface{}) error {
	entity := model.AdminRole{}
	if err := global.DB.Model(&entity).Where("role_id", req["role_id"]).Updates(req).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(添加)
 * @buildcode(false)
 */
func (p adminRole) Add(req *dto.AdminRoleAddReq) (uint, error) {
	entity := model.AdminRole{}
	entity.Name = req.Name
	entity.Status = req.Status
	entity.Description = req.Description
	entity.Access = req.Access

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	return entity.RoleId, nil
}

/**
 * @description(修改)
 * @buildcode(false)
 */
func (p adminRole) Update(req *dto.AdminRoleUpdateReq) error {
	entity := model.AdminRole{
		RoleId: req.RoleId,
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	entity.Name = req.Name
	entity.Status = req.Status
	entity.Description = req.Description
	entity.Access = req.Access

	field := conv.StringToSlice("name,status,description,access", ",")

	if err := global.DB.Select(field).Updates(&entity).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (p adminRole) GetUpdateInfo(req *dto.AdminRoleGetUpdateInfoReq) (*model.AdminRole, error) {
	entity := model.AdminRole{}
	if err := global.DB.Where("role_id = ?", req.RoleId).Limit(1).First(&entity).Error; err != nil {
		return nil, err
	}
	return &entity, nil
}

/**
 * @description(删除)
 * @buildcode(false)
 */
func (p adminRole) Delete(req *dto.AdminRoleDeleteReq) error {
	ids := conv.Slice(req.RoleId)
	if len(ids) > 1 {
		return errors.New("禁止批量删除")
	}
	if conv.Int(req.RoleId) == 1 {
		return errors.New("超级管理员禁止删除")
	}
	query := global.DB.Where("role_id IN ?", ids)
	if err := query.Delete(&model.AdminRole{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p adminRole) Detail(req *dto.AdminRoleDetailReq) (*vo.AdminRoleDetail, error) {
	var info vo.AdminRoleDetail
	query := global.DB.Model(&model.AdminRole{})
	if err := query.Where("role_id = ?", req.RoleId).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}
