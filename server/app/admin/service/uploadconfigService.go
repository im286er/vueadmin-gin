package service

import (
	"fmt"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/app/admin/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
)

var AdminUploadconfigService = new(adminUploadconfig)

type adminUploadconfig struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p adminUploadconfig) GetPageList(req *dto.AdminUploadconfigPageReq) ([]*vo.AdminUploadconfigList, int64, error) {
	var (
		entity model.AdminUploadconfig
		list   []*vo.AdminUploadconfigList
		count  int64
	)
	query := global.DB.Model(&entity)
	if !conv.IsEmpty(req.Id) {
		query = query.Where("id = ?", req.Id)
	}
	if !conv.IsEmpty(req.Title) {
		query = query.Where("title = ?", req.Title)
	}
	if !conv.IsEmpty(req.ThumbStatus) {
		query = query.Where("thumb_status = ?", req.ThumbStatus)
	}
	order := "id desc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)

	return list, count, nil
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (p adminUploadconfig) UpdateExt(req map[string]interface{}) error {
	entity := model.AdminUploadconfig{}
	if err := global.DB.Model(&entity).Where("id", req["id"]).Updates(req).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (p adminUploadconfig) Add(req *dto.AdminUploadconfigAddReq) (uint, error) {
	entity := model.AdminUploadconfig{}
	entity.Title = req.Title
	entity.ThumbStatus = req.ThumbStatus
	entity.ThumbWidth = req.ThumbWidth
	entity.ThumbHeight = req.ThumbHeight
	entity.ThumbType = req.ThumbType

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	return entity.Id, nil
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (p adminUploadconfig) Update(req *dto.AdminUploadconfigUpdateReq) error {
	entity := model.AdminUploadconfig{
		Id: req.Id,
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	entity.Title = req.Title
	entity.ThumbStatus = req.ThumbStatus
	entity.ThumbWidth = req.ThumbWidth
	entity.ThumbHeight = req.ThumbHeight
	entity.ThumbType = req.ThumbType

	field := conv.StringToSlice("title,thumb_status,thumb_width,thumb_height,thumb_type", ",")

	if err := global.DB.Select(field).Updates(&entity).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (p adminUploadconfig) GetUpdateInfo(req *dto.AdminUploadconfigGetUpdateInfoReq) (*model.AdminUploadconfig, error) {
	entity := model.AdminUploadconfig{}
	if err := global.DB.Where("id = ?", req.Id).Limit(1).First(&entity).Error; err != nil {
		return nil, err
	}
	return &entity, nil
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (p adminUploadconfig) Delete(req *dto.AdminUploadconfigDeleteReq) error {
	query := global.DB.Where("id IN ?", conv.Slice(req.Id))
	if err := query.Delete(&model.AdminUploadconfig{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p adminUploadconfig) Detail(req *dto.AdminUploadconfigDetailReq) (*vo.AdminUploadconfigDetail, error) {
	var info vo.AdminUploadconfigDetail
	query := global.DB.Model(&model.AdminUploadconfig{})
	if err := query.Where("id = ?", req.Id).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}
