package service

import (
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/app/admin/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
	"path/filepath"
)

var AdminFileService = new(adminFile)

type adminFile struct{}

// 数据列表
func (adminFile) GetPageList(req *dto.AdminFilePageReq) ([]*vo.AdminFileList, int64, error) {
	var (
		entity model.AdminFile
		list   []*vo.AdminFileList
		count  int64
	)
	query := global.DB

	order := "id desc"
	if err := query.Model(&entity).Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Model(&entity).Count(&count)

	return list, count, nil
}

// 添加
func (adminFile) Add(req *dto.AdminFileAddReq) (uint, error) {
	entity := model.AdminFile{}
	entity.Filepath = req.Filepath
	entity.Hash = conv.StringToSlice(filepath.Base(req.Filepath), ".")[0]
	entity.CreateTime = conv.NowUnix()
	entity.Disk = req.Disk

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	return entity.Id, nil
}

// 删除
func (adminFile) Delete(req *dto.AdminFileDeleteReq) error {
	query := global.DB.Where("filepath IN ?", req.Filepath)
	if err := query.Delete(&model.AdminFile{}).Error; err != nil {
		return err
	}
	return nil
}
