package service

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/app/admin/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/pkg/cache"
	"github.com/vueadmin/utils/conv"
	"time"
)

var AdminLogService = new(adminLog)

type adminLog struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p adminLog) GetPageList(req *dto.AdminLogPageReq) ([]*vo.AdminLogList, int64, error) {
	var (
		entity model.AdminLog
		list   []*vo.AdminLogList
		count  int64
	)
	query := global.DB.Model(&entity)
	if !conv.IsEmpty(req.Id) {
		query = query.Where("id = ?", req.Id)
	}
	if !conv.IsEmpty(req.ApplicationName) {
		query = query.Where("application_name = ?", req.ApplicationName)
	}
	if !conv.IsEmpty(req.Type) {
		query = query.Where("type = ?", req.Type)
	}
	if !conv.IsEmpty(req.Username) {
		query = query.Where("username = ?", req.Username)
	}
	if !conv.IsEmpty(req.CreateTime) {
		query = query.Where("create_time BETWEEN ? AND ?", conv.UnixTime(req.CreateTime[0]), conv.UnixTime(req.CreateTime[1]))
	}
	order := "id desc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)

	return list, count, nil
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (p adminLog) Delete(req *dto.AdminLogDeleteReq) error {
	query := global.DB.Where("id IN ?", conv.Slice(req.Id))
	if err := query.Delete(&model.AdminLog{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p adminLog) Detail(req *dto.AdminLogDetailReq) (*vo.AdminLogDetail, error) {
	var info vo.AdminLogDetail
	query := global.DB.Model(&model.AdminLog{})
	if err := query.Where("id = ?", req.Id).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}

/**
 * @description(导出)
 * @buildcode(true)
 */
func (p adminLog) Export(req *dto.AdminLogPageReq) ([]*vo.AdminLogExport, int64, error) {
	var (
		entity model.AdminLog
		list   []*vo.AdminLogExport
		count  int64
	)
	query := global.DB
	if !conv.IsEmpty(req.Id) {
		query = query.Where("id = ?", req.Id)
	}
	if !conv.IsEmpty(req.ApplicationName) {
		query = query.Where("application_name = ?", req.ApplicationName)
	}
	if !conv.IsEmpty(req.Type) {
		query = query.Where("type = ?", req.Type)
	}
	if !conv.IsEmpty(req.Username) {
		query = query.Where("username = ?", req.Username)
	}
	if !conv.IsEmpty(req.CreateTime) {
		query = query.Where("create_time BETWEEN ? AND ?", conv.UnixTime(req.CreateTime[0]), conv.UnixTime(req.CreateTime[1]))
	}
	order := "id desc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	if err := query.Model(&entity).Order(order).Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	for k, v := range list {
		list[k].Type = conv.GetItemVal(v.Type, `[{"key":"登录日志","label_color":"info","val":"1"},{"key":"操作日志","label_color":"warning","val":"2"},{"key":"异常日志","label_color":"danger","val":"3"}]`)
		list[k].CreateTime = conv.DateTime(conv.Int64(v.CreateTime))
	}
	if req.Page == 1 {
		cache.Gocache.Delete("log_count")
	}
	cache_count := conv.Int64(cache.Gocache.Getx("log_count"))
	if cache_count == 0 {
		query.Model(&entity).Count(&count)
		cache.Gocache.Setx("log_count", count, 300*time.Second)
	} else {
		count = cache_count
	}
	return list, count, nil
}

/**
 * @description(查询sql下拉字段列表)
 * @buildcode(true)
 */
func (p adminLog) GetFieldList(c *gin.Context) (map[string]interface{}, error) {
	var (
		ApplicationName []*vo.AdminLogApplicationName
	)
	if err := global.DB.Raw(`select app_dir,application_name from cd_application`).Scan(&ApplicationName).Error; err != nil {
		return nil, err
	}
	data := make(map[string]interface{})
	data["application_names"] = ApplicationName
	return data, nil
}
