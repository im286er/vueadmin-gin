package service

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/dto"
	"github.com/vueadmin/app/admin/model"
	"github.com/vueadmin/app/admin/vo"
	"github.com/vueadmin/global"
	"github.com/vueadmin/utils/conv"
	"gorm.io/gorm"
)

var AdminLinkService = new(adminLink)

type adminLink struct{}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
func (p adminLink) GetPageList(req *dto.AdminLinkPageReq) ([]*vo.AdminLinkList, int64, error) {
	var (
		entity model.AdminLink
		list   []*vo.AdminLinkList
		count  int64
	)
	query := global.DB.Model(&entity)
	if !conv.IsEmpty(req.LinkId) {
		query = query.Where("link_id = ?", req.LinkId)
	}
	if !conv.IsEmpty(req.Title) {
		query = query.Where("title = ?", req.Title)
	}
	if !conv.IsEmpty(req.LinkcataId) {
		query = query.Where("linkcata_id = ?", req.LinkcataId)
	}
	if !conv.IsEmpty(req.Status) {
		query = query.Where("status = ?", req.Status)
	}
	if !conv.IsEmpty(req.CreateTime) {
		query = query.Where("create_time BETWEEN ? AND ?", conv.UnixTime(req.CreateTime[0]), conv.UnixTime(req.CreateTime[1]))
	}
	order := "sortid asc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	query = query.Preload("Linkcata", func(db *gorm.DB) *gorm.DB {
		return db.Select("linkcata_id,class_name")
	})
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)

	return list, count, nil
}

/**
 * @description(修改排序开关)
 * @buildcode(true)
 */
func (p adminLink) UpdateExt(req map[string]interface{}) error {
	entity := model.AdminLink{}
	if err := global.DB.Model(&entity).Where("link_id", req["link_id"]).Updates(req).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(添加)
 * @buildcode(true)
 */
func (p adminLink) Add(req *dto.AdminLinkAddReq) (uint, error) {
	entity := model.AdminLink{}
	entity.Title = req.Title
	entity.Url = req.Url
	entity.LinkcataId = req.LinkcataId
	entity.Logo = req.Logo
	entity.Status = req.Status
	entity.Sortid = req.Sortid
	entity.CreateTime = conv.NowUnix()

	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	if entity.Sortid == 0 {
		if err := global.DB.Model(&entity).Update("sortid", entity.LinkId).Error; err != nil {
			return 0, err
		}
	}
	return entity.LinkId, nil
}

/**
 * @description(修改)
 * @buildcode(true)
 */
func (p adminLink) Update(req *dto.AdminLinkUpdateReq) error {
	entity := model.AdminLink{
		LinkId: req.LinkId,
	}
	if err := global.DB.Limit(1).First(&entity).Error; err != nil {
		return err
	}
	entity.Title = req.Title
	entity.Url = req.Url
	entity.LinkcataId = req.LinkcataId
	entity.Logo = req.Logo
	entity.Status = req.Status
	entity.Sortid = req.Sortid
	entity.CreateTime = conv.UnixTime(req.CreateTime)

	field := conv.StringToSlice("title,url,linkcata_id,logo,status,sortid,create_time", ",")

	if err := global.DB.Select(field).Updates(&entity).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(获取修改详情)
 * @buildcode(true)
 */
func (p adminLink) GetUpdateInfo(req *dto.AdminLinkGetUpdateInfoReq) (*model.AdminLink, error) {
	entity := model.AdminLink{}
	if err := global.DB.Where("link_id = ?", req.LinkId).Limit(1).First(&entity).Error; err != nil {
		return nil, err
	}
	return &entity, nil
}

/**
 * @description(删除)
 * @buildcode(true)
 */
func (p adminLink) Delete(req *dto.AdminLinkDeleteReq) error {
	query := global.DB.Where("link_id IN ?", conv.Slice(req.LinkId))
	if err := query.Delete(&model.AdminLink{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description(查看详情)
 * @buildcode(true)
 */
func (p adminLink) Detail(req *dto.AdminLinkDetailReq) (*vo.AdminLinkDetail, error) {
	var info vo.AdminLinkDetail
	query := global.DB.Model(&model.AdminLink{})
	query = query.Preload("Linkcata", func(db *gorm.DB) *gorm.DB {
		return db.Select("linkcata_id,class_name")
	})
	if err := query.Where("link_id = ?", req.LinkId).Limit(1).First(&info).Error; err != nil {
		return nil, err
	}
	return &info, nil
}

/**
 * @description(查询sql下拉字段列表)
 * @buildcode(true)
 */
func (p adminLink) GetFieldList(c *gin.Context) (map[string]interface{}, error) {
	var (
		LinkcataId []*vo.AdminLinkLinkcataId
	)
	if err := global.DB.Raw(`select linkcata_id,class_name from cd_linkcata`).Scan(&LinkcataId).Error; err != nil {
		return nil, err
	}
	data := make(map[string]interface{})
	data["linkcata_ids"] = LinkcataId
	return data, nil
}
