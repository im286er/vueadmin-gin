package middleware

import (
	"github.com/gin-gonic/gin"
	"github.com/vueadmin/app/admin/service"
	"github.com/vueadmin/pkg/cache"
	"github.com/vueadmin/utils/conv"
	"time"
)

// 获取系统配置写入上下文
func BeforAction() gin.HandlerFunc {
	return func(c *gin.Context) {
		data := cache.New().Remember("base_config", 30*time.Second, func() string {
			config, err := service.AdminBaseconfigService.Index()
			if err != nil {
				panic(err)
			}
			return conv.ObjectToString(config)
		})
		for key, val := range conv.StringToObject(data) {
			c.Set(key, val)
		}
		c.Next()
	}
}
