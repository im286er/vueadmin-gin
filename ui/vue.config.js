module.exports = {
    devServer: {
      host: '127.0.0.1',
      port: 3333,
      open: false,
    },
    outputDir: 'dist',
    assetsDir: 'static',
    productionSourceMap:false,
    
  }