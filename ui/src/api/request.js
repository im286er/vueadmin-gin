import axios from 'axios'
import { Message,MessageBox} from 'element-ui'
import store from '../store'

//请求地址
axios.defaults.baseURL = 'http://127.0.0.1:8080'
//设置超时时间
axios.defaults.timeout = 5000

axios.interceptors.request.use((config) => {
    const token = localStorage.getItem("adminToken")
    if(token){
        config.headers['Authorization'] =  token 
    }
    return config;
  }, (error) => {
    return Promise.reject(error);
})

// 响应拦截器（处理响应数据）
axios.interceptors.response.use(
    (res) => {
        const ret = res.data
        if(ret.status !== 200 && !ret.key && !ret.code && !ret.Location){
            if(ret.status == 101){
                MessageBox.confirm(ret.msg, '系统提示', {
                    confirmButtonText: '重新登录',
                    cancelButtonText: '取消',
                    type: 'warning'
                  }
                ).then(() => {
                    store.dispatch('logout').then(() => {
                        location.href = '/';
                    })
                })
            }else{
                Message({
                    showClose: true,
                    message: ret.msg || 'Server error',
                    type: 'error',
                    duration: 2000
                })
            }
            reject(ret.msg)
        }
        return res
    }, 
    (error) => {
        Message({
            showClose: true,
            message: error.message,
            type: 'error',
            duration: 2000
        })
        return Promise.reject(error);
    }
);
