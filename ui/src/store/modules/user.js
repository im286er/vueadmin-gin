import axios from 'axios'
import request from '@/api/request'


export default {
    state :{
        addRoutes:[],
        menu:[],
        actions:[],
        username:'',
        headimg:'',
        site_title:'',
        show_notice:true,
    },
    mutations :{
        setRoute(state,route){
            state.addRoutes = route
        },
        setMenu(state,menu){
            state.menu = menu
        },
        setActions(state,actions){
            state.actions = actions
        },
        setUserInfo(state,val){
            state.username = val.user
            state.headimg = val.headimg
            state.role_id = val.role_id
            state.site_title = val.site_title
            state.show_notice = val.show_notice
        }
    },
    actions :{
        login({commit},userInfo) {
            return new Promise((resolve, reject) => {
                axios.post('/admin/Login/login',userInfo).then(res => {
                    localStorage.setItem("adminToken", res.data.data)
                    resolve()
                }).catch(error => {
                    reject(error)
                })
            })
        },
        getUserInfo({commit}){
            return new Promise((resolve, reject) => {
                axios.post('/admin/Base/getSiteInfo').then(res => {
                    if(res.data.status == 200){
                        let routes = res.data.components
                        let baseRoutes = [
                            {
                                path:'/',
                                component: (resolve) => require(['@/layout/admin/default/Main'], resolve),
                                children:[]
                            },
                        ]
                        routes.forEach(item =>{
                            if(item.component_path){
                                item.component = (resolve) => require([`@/views/${item.component_path}`],resolve)
                                baseRoutes[0].children.push(item)
                            }
                        })
                        commit('setRoute',baseRoutes.concat({path:'*',redirect:'/404'}))
                        commit('setMenu',res.data.menu)
                        commit('setActions',res.data.actions)
                        commit('setUserInfo',res.data.data)
                    }
                    resolve()
                }).catch(error => {
                    reject(error)
                })
            })
        },
        logout({commit}){
            return new Promise((resolve, reject) => {
                axios.post('/admin/Index/logout').then(res => {
                    if(res.status == 200){
                        localStorage.setItem("adminToken", "")
                        commit('setRoute',[])
                        commit('setMenu',[])
                        commit('setActions',[])
                        commit('setUserInfo',{})
                    }
                    resolve()
                }).catch(error => {
                    reject(error)
                })
            })
        }
        
    }
}